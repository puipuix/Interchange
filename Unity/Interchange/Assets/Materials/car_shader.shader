﻿Shader "Custom/Car Shader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BaseColor ("Color", Color) = (1,1,1,1)
		_BlinkTime ("Blink Time", Range(0.0,2.0)) = 1
		_BlinkOn ("Blink On", Range(0.0,1.0)) = 0.5
		_BlinkLeft ("Blink Left", Int) = 0
		_BlinkRight ("Blink Right", Int) = 0
		_Brakes ("Brakes", Int) = 0
		_Lights ("Lights", Int) = 0
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			static const fixed3 BLINK_ON_COLOR = fixed3(1.0,0.5,0.05);
			static const fixed3 BLINK_OFF_COLOR = fixed3(0.3,0.15,0.05);
			static const fixed3 BRAKES_ON_COLOR = fixed3(1.0,0.05,0.05);
			static const fixed3 BRAKES_OFF_COLOR = fixed3(0.2,0.05,0.05);
			static const fixed3 LIGHTS_ON_COLOR = fixed3(0.95,0.95,0.5);
			static const fixed3 LIGHTS_OFF_COLOR = fixed3(0.25,0.2,0.15);

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			float4 _BaseColor;
			float _BlinkTime;
			float _BlinkOn;
			float _BlinkLeft;
			float _BlinkRight;
			float _Brakes;
			float _Lights;

			fixed4 frag (v2f i) : SV_Target
			{
					float2 UV = i.uv;
					float4 text = tex2D(_MainTex, i.uv);
					float4 COLOR = text;
					
					// Check lights
					if (text.b == 1.0) {
						// front
						if (UV.x > 0.5){
							if (_Lights) {
								COLOR.rgb = LIGHTS_ON_COLOR;
							} else {
								COLOR.rgb = LIGHTS_OFF_COLOR;
							}
							// back
						} else {
							if (_Lights) {
								COLOR.rgb = BRAKES_ON_COLOR;
							} else {
								COLOR.rgb = BRAKES_OFF_COLOR;
							}
						}
					}
					// check brakes
					if (text.g == 1.0) {
						if (_Brakes)	{
							COLOR.rgb = BRAKES_ON_COLOR;
						} else if (text.b < 1.0) { // don't override if already set by lights
							COLOR.rgb = BRAKES_OFF_COLOR;
						}
					}
					
					// blinkers
					if (text.r == 1.0) {
						// time to blink and (is pixel left or right blinker)
						if ((fmod(_Time.y / _BlinkTime, 1.0) < _BlinkOn) && ((_BlinkLeft && UV.y > 0.5) || (_BlinkRight && UV.y < 0.5))) {
							COLOR.rgb = BLINK_ON_COLOR;
						} else {
							if (text.b < 1.0) {
								COLOR.rgb = BLINK_OFF_COLOR;
							}
						}
					}

					// clip transparency
					clip(COLOR.a - 0.01);
					return COLOR;
			}
			ENDCG
		}
	}
}
