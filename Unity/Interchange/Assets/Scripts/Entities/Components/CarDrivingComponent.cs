﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;

using UnityEngine;

namespace Interchange.Entities.Components
{
    [GenerateAuthoringComponent]
    public struct CarDrivingComponent : IComponentData
    {
        [HideInInspector]
        public float Steering;
        [HideInInspector]
        public float Accelerating;
        [HideInInspector]
        public float Breaking;
        [HideInInspector]
        public float Z;
    }
}