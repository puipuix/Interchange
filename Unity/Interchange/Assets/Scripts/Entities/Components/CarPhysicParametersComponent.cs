using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Interchange.Entities.Components
{
    [GenerateAuthoringComponent]
    public struct CarPhysicParametersComponent : IComponentData
    {
        [Header("Shape")]
        public float FrontWheelPosition;
        public float BackWheelPosition;

        [Header("Steering")]
        public float HighSpeed;
        public float HighSpeedSteeringFactor;
        public float SteeringAngle;

        [Header("Forces")]
        public float FrictionForce;
        public float EngineForce;
        public float BreakingForce;

        [Header("AI")]
        public float HeadingForce;
        public float LookAheadDistance;
        public float CurveToBreaking;

        public float2 Forward(Quaternion rotation)
        {
            Vector3 result = rotation * Vector3.right;
            return new float2(result.x, result.y);
        }

        public float GetSteeringAngleAtSpeed(float velocity)
        {
            return math.lerp(SteeringAngle, SteeringAngle * HighSpeedSteeringFactor, math.min(1f, velocity / HighSpeed));
        }
    }
}