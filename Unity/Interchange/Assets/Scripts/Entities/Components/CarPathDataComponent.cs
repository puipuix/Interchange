using System.Collections;
using System.Collections.Generic;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Interchange.Entities.Components
{
    [GenerateAuthoringComponent]
    public struct CarPathDataComponent : IComponentData
    {
        [HideInInspector]
        public int PathIndex;
    }
}