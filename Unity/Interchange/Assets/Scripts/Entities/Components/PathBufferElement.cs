using JetBrains.Annotations;

using System.Collections;
using System.Collections.Generic;

using Unity.Entities;
using Unity.Mathematics;

using UnityEngine;

namespace Interchange.Entities.Components
{
    [GenerateAuthoringComponent]
    public struct PathBufferElement : IBufferElementData
    {
        public float3 Position;

        public static implicit operator float3(PathBufferElement e) => e.Position;
        public static implicit operator float2(PathBufferElement e) => e.XY;

        public float Z => Position.z;
        public float2 XY => new float2(Position.x, Position.y);
    }
}