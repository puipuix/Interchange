﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;

using UnityEngine;

namespace Interchange.Entities.Tags
{
    [GenerateAuthoringComponent]
    public struct PlayerControlTag : IComponentData { }
}