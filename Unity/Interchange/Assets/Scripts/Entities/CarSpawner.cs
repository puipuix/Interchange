using Interchange.Entities.Components;
using Interchange.Entities.Tags;

using System.Collections;
using System.Collections.Generic;

using Unity.Entities;
using Unity.Transforms;

using UnityEditor;

using UnityEngine;
using UnityEngine.Assertions;

namespace Interchange.Entities
{
    public class CarSpawner : MonoBehaviour
    {
        public GameObject Prefab;
        public int AICount;

        private Entity _entity_prefab;
        private BlobAssetStore _asset;

        // Start is called before the first frame update
        void Start()
        {
            Assert.IsNotNull(Prefab);
            _asset = new BlobAssetStore();
            var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, _asset);
            _entity_prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(Prefab, settings);

            // TODO: REMOVE
            Spawn(true);
            for (int i = 0; i < AICount; i++)
            {
                Spawn();
            }
        }
        void OnDrawGizmos()
        {
            // Draw a yellow sphere at the transform's position
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.rotation * Vector3.right);
        }

        private void Spawn(bool isPlayer = false)
        {
            EntityManager manager = World.DefaultGameObjectInjectionWorld.EntityManager;
            Entity instance = manager.Instantiate(_entity_prefab);

            // TODO Remove random
            manager.SetComponentData(instance, new Translation() { Value = transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10)) });
            manager.SetComponentData(instance, new Rotation() { Value = transform.rotation });
            if (isPlayer)
            {
                manager.AddComponent<PlayerControlTag>(instance);
            }
            else
            {
                if (!manager.HasChunkComponent<PathBufferElement>(instance))
                {
                    manager.AddBuffer<PathBufferElement>(instance);
                }
            }
        }

        void OnDestroy()
        {
            _asset.Dispose();
        }
    }
}