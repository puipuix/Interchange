﻿using Interchange.Entities.Tags;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

using UnityEngine;

namespace Interchange.Entities.Systems
{
    public class DebugVelocitySystem : SystemBase
    {
        private EntityQuery _query;
        protected override void OnCreate()
        {
            base.OnCreate();
            _query = GetEntityQuery(typeof(PhysicsVelocity), typeof(PlayerControlTag));
            Enabled = false;
        }
        protected override void OnUpdate()
        {
            var data = _query.ToComponentDataArray<PhysicsVelocity>(Allocator.Temp);

            StringBuilder b = new StringBuilder("Velocity: (").Append(data.Length).AppendLine(")");
            for (int i = 0; i < data.Length; i++)
            {
                b.Append(i).Append(": ").Append(math.length(data[i].Linear) * 3.6).AppendLine("km/h");
            }
            Debug.Log(b.ToString());
        }
    }
}