﻿using Interchange.Entities.Components;
using Interchange.Entities.Tags;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;

using UnityEngine;

namespace Interchange.Entities.Systems
{
    public class CarPlayerInputSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            float acc = 0, br = 0, st = 0;

            if (Input.GetKey(KeyCode.Z))
            {
                acc = 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                br = 1;
            }
            st = (Input.GetKey(KeyCode.Q) ? 1 : 0) + (Input.GetKey(KeyCode.D) ? -1 : 0);

            Entities
                .WithName("SetPlayerInputs")
                .WithAll<PlayerControlTag>()
                .ForEach((ref CarDrivingComponent input) =>
                {
                    input.Accelerating = acc;
                    input.Breaking = br;
                    input.Steering = st;
                    input.Z = 0; // TODO ?
            })
                .ScheduleParallel();
        }
    }
}