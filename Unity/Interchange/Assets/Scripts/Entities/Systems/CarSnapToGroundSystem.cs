using Interchange.Entities.Components;

using System.Collections;
using System.Collections.Generic;

using Unity.Entities;
using Unity.Transforms;

using UnityEngine;

namespace Interchange.Entities.Systems
{
    public class CarSnapToGroundSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithName("SnapToGround")
                .ForEach((ref Translation translation, in CarDrivingComponent input) =>
                {
                    translation.Value.z = input.Z;
                })
                .ScheduleParallel();
        }
    }
}
