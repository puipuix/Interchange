using Interchange.Entities.Components;

using System.Collections;
using System.Collections.Generic;
using System.Text;

using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Transforms;

using UnityEngine;

namespace Interchange.Entities.Systems
{
    public class CarPhysicSystem : SystemBase
    {
        public const float MIN_VELOCITY_TO_MOVE = 0.001f;
        public const float HIGH_FRICTION_VELOCITY = 1;

        protected override void OnUpdate()
        {
            float DELTA_TIME = Time.DeltaTime;
            Vector3 UP = Vector3.forward;
            Entities
                    .WithName("ApplyCarInputs")
                    .ForEach((ref PhysicsVelocity data, ref Rotation rotation, in CarDrivingComponent input, in CarPhysicParametersComponent param) =>
                    {
                        // INPUTS
                        float2 forward = param.Forward(rotation.Value);
                        float2 velocity = new float2(data.Linear.x, data.Linear.y);
                        float velocity_scalar = math.length(velocity);

                        float acceleration = input.Accelerating * param.EngineForce - input.Breaking * param.BreakingForce;

                        float2 rear_wheel = forward * param.BackWheelPosition;
                        float2 front_wheel = forward * param.FrontWheelPosition;

                        rear_wheel += velocity * DELTA_TIME;

                        float steer = input.Steering * param.GetSteeringAngleAtSpeed(velocity_scalar);

                        Vector3 result = new Vector3(velocity.x, velocity.y, 0);
                        result = Quaternion.Euler(0, 0, steer) * result * DELTA_TIME;
                        front_wheel += new float2(result.x, result.y);

                        float2 heading = math.normalize(front_wheel - rear_wheel);
                        velocity = heading * velocity_scalar;

                        // FRICTIONS
                        if (velocity_scalar < MIN_VELOCITY_TO_MOVE)
                        {
                            velocity = float2.zero;
                        }

                        float friction = velocity_scalar * param.FrictionForce;

                        if (velocity_scalar < HIGH_FRICTION_VELOCITY)
                        {
                            friction *= 3;
                        }

                        acceleration -= friction;

                        // MOVE

                        if (velocity_scalar < -acceleration * DELTA_TIME)
                        {
                            velocity = float2.zero;
                            velocity_scalar = 0;
                        }
                        else
                        {
                            velocity += forward * acceleration * DELTA_TIME;
                            velocity_scalar = math.length(velocity);

                            data.Linear = new float3(velocity, 0);

                            if (velocity_scalar > MIN_VELOCITY_TO_MOVE)
                            {
                                // Align car rotation to the velocity
                                rotation.Value = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.right, velocity), UP);
                            }
                        }
                    })
                    .ScheduleParallel();
        }
    }
}
