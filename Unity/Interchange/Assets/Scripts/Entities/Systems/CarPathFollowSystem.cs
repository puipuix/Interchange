﻿using Interchange.Entities.Components;
using Interchange.Entities.Tags;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

using UnityEngine;

namespace Interchange.Entities.Systems
{
    public class CarPathFollowSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            float2 POW = new float2(1, 1); // odd number
            Entities
                .WithName("PathFollow")
                .WithNone<PlayerControlTag>()
                .ForEach((ref CarPathDataComponent data, ref CarDrivingComponent input, in PhysicsVelocity physics, in Rotation rotation, in Translation translation, in CarPhysicParametersComponent param, in DynamicBuffer<PathBufferElement> buffer) =>
                {
                    Path path = buffer;
                    if (path.IsSet())
                    {
                        float2 forward = param.Forward(rotation.Value);
                        float2 position = new float2(translation.Value.x, translation.Value.y);
                        float2 velocity = new float2(physics.Linear.x, physics.Linear.y);
                        float velocity_scalar = math.length(velocity);


                        // Find closest point
                        data.PathIndex = path.GetNextIndex(data.PathIndex, position);

                        // Steering
                        float2 dir = math.pow((path.GetPosition(data.PathIndex) - position) * param.HeadingForce, POW);
                        dir = math.normalize(dir);
                        input.Steering = Vector2.SignedAngle(forward, dir) / param.GetSteeringAngleAtSpeed(velocity_scalar);
                        input.Steering = math.clamp(input.Steering, -1, 1);

                        // Breaking
                        float curve = path.GetCurve(data.PathIndex, forward, param.LookAheadDistance);
                        curve = math.min(1, curve * 2); // 90° is an high curve
                        input.Breaking = math.lerp(0, param.CurveToBreaking, curve);

                        // Accelerating
                        input.Accelerating = 1; // TODO collision

                        // Find average Z position
                        input.Z = path.GetLerpZ(data.PathIndex, position);
                    } else
                    {
                        input.Accelerating = 0;
                        input.Breaking = 1;
                        input.Steering = 0;
                        input.Z = 0;
                    }                    
                })
                .ScheduleParallel();
        }
    }
}