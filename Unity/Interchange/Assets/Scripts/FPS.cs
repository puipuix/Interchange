﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Interchange
{
    public class FPS : MonoBehaviour
    {
        public Text text;
        public float refresh_time = 1;
        public string custom_text;

        private float _timer = 0;

        // Start is called before the first frame update
        void Start()
        {
            text.text = "-- FPS";
        }

        // Update is called once per frame
        void Update()
        {
            if (_timer < 0)
            {
                _timer = refresh_time;
                text.text = (1.0f / Time.smoothDeltaTime) + " FPS\n" + (custom_text ?? "");
            }
            else
            {
                _timer -= Time.deltaTime;
            }
        }
    }
}