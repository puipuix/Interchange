﻿using Interchange.Entities.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Unity.Entities;
using Unity.Mathematics;

namespace Interchange
{
    public struct Path
    {
        public const float TRIP_COMPLETION_TO_REACH_NEXT_POINT = 0.95f;
        public const float DISTANCE_TO_REACH_NEXT_POINT = 1f;

        public DynamicBuffer<PathBufferElement> Buffer;

        public static implicit operator Path(DynamicBuffer<PathBufferElement> buf) => new Path { Buffer = buf };
        public static implicit operator DynamicBuffer<PathBufferElement>(Path p) => p.Buffer;

        public bool IsSet()
        {
            return Buffer.Length > 1;
        }

        private void Assert()
        {
            Unity.Assertions.Assert.IsTrue(IsSet(), "Buffer empty");
        }

        private int GetIndex(int index, int min = 0, int max = -1)
        {
            return math.clamp(index, min, max == -1 ? Buffer.Length - 1 : max);
        }

        public float2 GetPosition(int index)
        {
            Assert();
            return Buffer[GetIndex(index)];
        }
        public float2 GetDirection(int index)
        {
            Assert();
            index = GetIndex(index, max: Buffer.Length - 2);
            if (Buffer.Length > 2 && index > 0)
            {
                float2 a = Buffer[index - 1];
                float2 b = Buffer[index];
                float2 c = Buffer[index + 1];
                return (math.normalize(c - b) + math.normalize(b - a)) / 2;
            }
            else
            {
                float2 b = Buffer[index];
                float2 c = Buffer[index + 1];
                return math.normalize(c - b);
            }
        }

        private float DotToCurve(float dot) => 1 - (1 + dot) / 2;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>A value from 0 (linear) to 1 (high curve)</returns>
        public float GetCurve(int index, float2 forward, float look_distance)
        {
            Assert();
            index = math.max(index, 0);
            // if path is straight or if there are only one line left
            if (index >= Buffer.Length - 2)
            {
                return 0;
            }
            else
            {
                float curve = 0;
                float distance = 0;
                int i = index;
                float2 a, b, c;
                if (i == 0) // no previous for index 0 
                {
                    b = Buffer[index];
                    c = Buffer[index + 1];
                    curve = DotToCurve(math.dot(forward, math.normalize(c - b)));
                    distance = math.distance(b, c);
                    i++;
                }
                // will be shifted
                b = Buffer[i - 1];
                c = Buffer[i];
                while (distance < look_distance && i < Buffer.Length - 2)
                {
                    a = b;
                    b = c;
                    c = Buffer[i + 1];
                    float dot = math.dot(forward, (math.normalize(c - b) + math.normalize(b - a)) / 2);
                    Unity.Assertions.Assert.IsTrue(-1 <= dot && dot <= 1);
                    curve = math.max(curve, DotToCurve(dot));
                    distance += math.distance(b, c);
                    i++;
                }
                return curve;
            }
        }

        public PathTrip GetLerpData(int index, float2 position)
        {
            Assert();
            index = GetIndex(index);
            if (index == 0)
            {
                var c = Buffer[index];
                float distance = math.distance(position, c.XY);
                return new PathTrip()
                {
                    From = c,
                    To = c,
                    Completion = 0,
                    Distance = distance
                };
            }
            else
            {
                var c = Buffer[index];
                var p = Buffer[index - 1];
                float c_d = math.distance(c.XY, position);
                float p_d = math.distance(p.XY, position);
                return new PathTrip() { From = p, To = c, Completion = p_d / (p_d + c_d), Distance = c_d };
            }
        }

        public float GetLerpZ(int index, float2 position)
        {
            PathTrip trip = GetLerpData(index, position);
            return math.lerp(trip.From.Z, trip.To.Z, trip.Completion);
        }

        public int GetNextIndex(int index, float2 position)
        {
            PathTrip trip = GetLerpData(index, position);
            if (trip.Completion >= TRIP_COMPLETION_TO_REACH_NEXT_POINT || trip.Distance < DISTANCE_TO_REACH_NEXT_POINT)
            {
                return GetIndex(index + 1);
            }
            else
            {
                return index;
            }
        }
    }
}