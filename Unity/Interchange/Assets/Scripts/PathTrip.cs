﻿using Interchange.Entities.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interchange
{
    public struct PathTrip
    {
        public PathBufferElement To;
        public PathBufferElement From;
        public float Completion;
        public float Distance;
    }
}
