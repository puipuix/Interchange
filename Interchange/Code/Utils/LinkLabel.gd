extends RichTextLabel

# open links
func _ready():
	MyTools.print_if_error(connect("meta_clicked", self, "_on_meta_clicked"))

func _on_meta_clicked(meta):
	print("click")
	MyTools.print_if_error(OS.shell_open(str(meta)))
