extends RichTextLabel

class_name GDEFPS

# refresh display every x seconds
export var refresh_time : float = 1.0

# sort the key value using a priority
var _priority_keys = []
var _data = {}
var color = Color.yellow

var _ms_previous_process = OS.get_system_time_msecs()
var _ms_previous_physics = OS.get_system_time_msecs()

var _count_process = 0
var _count_physics = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	mouse_filter = Control.MOUSE_FILTER_IGNORE
	bbcode_enabled = true
	display("TPS", "--", 10002)
	display("FPS", "--", 10001)
	display("SPD", "--", 10000)
	update_text()
	_resize()

func sort(a,b):
	return _data[a].priority > _data[b].priority

func erase(key: String):
	_data.erase(key)
	_priority_keys.erase(key)
	
func display(key: String, value, priority: int = 0):
		var old = _data.get(key)
		# if key not found
		if old == null:
			# save the key, value and priority
			_data[key] = {"value":value,"priority":priority}
			_priority_keys.append(key)
			# sort priorities
			_priority_keys.sort_custom(self, "sort")
		else:
			# update text
			old.value = value
			# if priority is changed
			if old.priority != priority:
				old.priority = priority
				# sort priorities
				_priority_keys.sort_custom(self, "sort")

func _resize():
	var size = get_viewport().size
	margin_bottom = size.y * 2
	margin_right = size.x * 2

func _physics_process(delta):
	_count_physics += 1
	# ignore delta as it can be changed with Engine.time_scale
	var time = OS.get_system_time_msecs()
	var real_delta  : float = time - _ms_previous_physics
	# if time to update display
	if real_delta > refresh_time * 1000 and _count_physics > 0:
		real_delta /= _count_physics
		_count_physics = 0
		_ms_previous_physics = time
		display("TPS", "%.0f (%.3fms)" % [1000/max(1,real_delta), min(0.999,delta)*1000], 10002)

func _process(delta):
	_count_process += 1
	# ignore delta as it can be changed with Engine.time_scale
	var time = OS.get_system_time_msecs()
	var real_delta : float = time - _ms_previous_process
	# if time to update display
	if real_delta > refresh_time * 1000 and _count_process > 0:
		real_delta /= _count_process
		_count_process = 0
		_ms_previous_process = time
		display("FPS", "%.0f (%.3fms)" % [1000/max(1,real_delta), min(0.999,delta)*1000], 10001)
		display("SPD", "x%0.f" % [Engine.time_scale], 10000)
		update_text()
	
# format bbcode
func update_text():
	var formated = ""
	for key in _priority_keys:
		var value = _data[key].value
		formated += "[cell][b][right]%s[/right][/b]:[/cell][cell]%s[/cell]" % [key, str(value)]
	bbcode_text = "[code][color=#%s][table=2]%s[/table][/color][/code]" % [color.to_html(false), formated]
