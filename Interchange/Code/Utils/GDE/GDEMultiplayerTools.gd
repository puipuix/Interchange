extends Node


#	Get the GDEMultiplayer parent of the node
func find_multiplayer(node: Node) -> GDEMultiplayer:
	if node == null:
		return null
	elif node is GDEMultiplayer:
		return node as GDEMultiplayer
	else:
		return find_multiplayer(node.get_parent())
