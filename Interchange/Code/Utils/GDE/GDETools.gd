extends Node

# call_defered with seconds instead of 1 tick
func call_deferred_s(time: float, target: Object, method: String, args := []) -> SceneTreeTimer:
	var timer := get_tree().create_timer(time)
	timer.connect("timeout", target, method, args)
	return timer
