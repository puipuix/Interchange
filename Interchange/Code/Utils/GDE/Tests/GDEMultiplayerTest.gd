extends Node


func _ready():
	var server = GDEServer.new()
	var cli1 = GDEClient.new()
	var cli2 = GDEClient.new()
	var cli3 = GDEClient.new()
	var cli4 = GDEClient.new()
	var empty_data = GDEPlayerData.new()
	
	server.name = "Server"
	cli1.name = "Client1"
	cli2.name = "Client2"
	cli3.name = "Client3"
	cli4.name = "Client4"
	
	add_child(server)
	add_child(cli1)
	add_child(cli2)
	add_child(cli3)
	add_child(cli4)
	
	server.start_server(8080)
	cli1.connect_to_server("localhost", 8080, empty_data)
	cli2.connect_to_server("localhost", 8080, empty_data)
	cli4.connect_to_server("localhost", 8080, empty_data)
	GDETools.call_deferred_s(3, cli3, "connect_to_server", ["localhost", 8080, empty_data])
	GDETools.call_deferred_s(5, cli2, "leave_server")
	GDETools.call_deferred_s(7,server, "stop_server")
