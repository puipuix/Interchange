extends Node

var _refresh = -1.0

var refresh_time = 1.0
var _camera : Node = null
var _canvas = CanvasLayer.new()

signal camera_changed()
	
func _ready():
	call_deferred("set_camera", null)
	
func add_node(node: Node):
	_move_to(node, _canvas)
	
func remove_node(node: Node):
	_canvas.remove_child(node)
	
func _move_to(node: Node, target: Node):
	if node.get_parent() != null: node.get_parent().remove_child(node)
	target.add_child(node)
	
func set_camera(camera: Node):
	_camera = camera
	
	if _camera == null:
		_move_to(_canvas, get_tree().root)
	else:
		_move_to(_canvas, _camera)
	
	_refresh = -1.0
	
	print("Camera updated")
	emit_signal("camera_changed")
	
func _process(delta):
	# check if current camera is still active
	if _camera != null:
		if not (_camera.current and _camera.is_inside_tree()):
			set_camera(null)
		
	# if no camera is set, we will look for one every so often
	if _camera == null:
		if _refresh < 0:
			_refresh = refresh_time
			var to_do = []
			to_do.push_back(get_tree().root)
			while _camera == null and not to_do.empty():
				var current = to_do.pop_back()
				var ok = false
				if current is Camera or current is Camera2D:
					if current.current:
						set_camera(current)
						ok = true
				if not ok:
					to_do.append_array(current.get_children())
		else:
			_refresh -= delta
