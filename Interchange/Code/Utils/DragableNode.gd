extends Area2D

class_name DragableNode

var offset = Vector2.ZERO
var follow_mouse := false

export var locked = false

onready var _sprite = $Sprite

var displaying = true

signal position_changed()

static func get_layer():
	return 16

# save the offset to the mouse position and start following it if not locked
func start_follow_mouse():
	if not locked:
		follow_mouse = true
		offset = global_position - get_global_mouse_position()
		return true
	else:
		return false

func _process(_delta):
	# follow if visible and mouse pressed
	follow_mouse = follow_mouse and displaying and Input.is_action_pressed("move_object")
	if follow_mouse:
		var old = global_position
		global_position = get_global_mouse_position() + offset
		
		if old != global_position:
			emit_signal("position_changed")

func set_displaying(val: bool):
	displaying = val
	_sprite.visible = val
