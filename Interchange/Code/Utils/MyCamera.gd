extends Camera2D

class_name MyCamera

export(float,0,10) var movement_speed := 5
export(float,0,10) var zoom_speed := 2.0

var _target_pos
var _target_zoom
var _target

var _old_time
var _mouse_start_position
var _camera_start_position

func _ready():
	_target_zoom = zoom
	_target_pos = global_position
	_old_time = OS.get_system_time_msecs()

# start following a node
func move_to(target: Node2D):
	_target = target

func _physics_process(delta):
	# ignore delta
	var t = OS.get_system_time_msecs()
	delta = (t - _old_time) * 0.001
	_old_time = t
	var sm =  delta * smoothing_speed
	
	# move and zoom smoothly
	global_position = global_position.linear_interpolate(_target_pos, sm)
	zoom = zoom.move_toward(_target_zoom, sm)
	
	# update zoom
	if Input.is_action_just_released("zoom_in"):
		_target_zoom /= 1 + (zoom_speed * delta)
	if Input.is_action_just_released("zoom_out"):
		_target_zoom *= 1 + (zoom_speed * delta)
		
	# check if target still exist and follow it
	if _target != null:
		if is_instance_valid(_target):
			if _target.is_inside_tree():
				_target_pos = _target.global_position
			else:
				move_to(null)
		else:
			move_to(null)
	
	# save offset to drag camera
	if Input.is_action_just_pressed("select_object"):
		_mouse_start_position = get_global_mouse_position()
		_camera_start_position = global_position
		
	# drag camera
	if Input.is_action_pressed("select_object"):
		_target_pos = _camera_start_position + (_mouse_start_position - get_global_mouse_position())
	
	# key movement
	var move = Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
		)

	# set target position
	_target_pos += move * get_viewport_rect().size * zoom * movement_speed * delta
	
	if Input.is_action_just_pressed("center"):
			_target_pos = Vector2.ZERO
			move_to(null)
	
	if move != Vector2.ZERO:
		move_to(null)
	
