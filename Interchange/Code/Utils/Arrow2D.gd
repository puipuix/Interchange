extends Node2D

class_name Arrow2D

export var arrow_length := 40.0
export var arrow_width := 5.0
export var arrow_head := 10.0

export var _position := Vector2.ZERO
export var _direction := Vector2.LEFT

func set_from_direction(pos: Vector2, dir: Vector2):
	_position = to_local(pos)
	_direction = dir
	update()

func set_from_position(pos: Vector2, target_pos: Vector2):
	set_from_direction(pos, pos.direction_to(target_pos))
	
func _draw():
	print("draw")
	var head = _position + _direction * arrow_length
	draw_line(_position, head, Color.white, arrow_width)
	var dir = _direction.rotated(PI * 0.75)
	draw_line(head - dir * arrow_width * 0.5, head + dir * arrow_head, Color.white, arrow_width)
	dir = _direction.rotated(-PI * 0.75)
	draw_line(head - dir * arrow_width * 0.5, head + dir * arrow_head, Color.white, arrow_width)
