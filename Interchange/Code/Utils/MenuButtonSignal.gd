extends MenuButton

# emit popup signal
signal id_focused(id)
signal id_pressed(id)
signal index_pressed(index)
signal popup_hide()

func _ready():
	MyTools.print_if_error(get_popup().connect("id_focused", self, "_on_id_focused"))
	MyTools.print_if_error(get_popup().connect("id_pressed", self, "_on_id_pressed"))
	MyTools.print_if_error(get_popup().connect("index_pressed", self, "_on_index_pressed"))
	MyTools.print_if_error(get_popup().connect("popup_hide", self, "_on_popup_hide"))

func _on_id_focused(id: int):
	emit_signal("id_focused", id)

func _on_id_pressed(id: int):
	emit_signal("id_pressed", id)
	
func _on_index_pressed(index: int):
	emit_signal("index_pressed", index)
	
func _on_popup_hide():
	emit_signal("popup_hide")
