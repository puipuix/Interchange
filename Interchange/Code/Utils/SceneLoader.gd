extends Node

enum {LINK = 0, NODE = 1, SPAWN = 2, DESPAWN = 3, CTRL = 4, CAR = 5}

var scenes = [
	preload("res://Code/Paths/PathLink.tscn"),
	preload("res://Code/Paths/PathNode.tscn"),
	preload("res://Code/Paths/PathNodeSpawner.tscn"),
	preload("res://Code/Paths/PathNodeDespawner.tscn"),
	preload("res://Code/Paths/PathLinkControl.tscn"),
	preload("res://Code/Vehicles/Entities/CarVehicle.tscn"),
]

func instance(id: int):
	return scenes[id].instance()
