extends Node

class_name ProcessTimer

var process_time = 0.0
var physics_time = 0.0
var started = false

func reset():
	process_time = 0.0
	physics_time = 0.0

func restart():
	reset()
	started = true

func start():
	if not started:
		restart()

func stop():
	started = false

func _process(delta):
	if started:
		process_time += delta
	
func _physics_process(delta):
	if started:
		physics_time += delta

func get_process_duration(old: float) -> float:
	return process_time - old

func get_physics_duration(old: float) -> float:
	return physics_time - old
