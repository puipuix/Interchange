extends Node

const HEIGHT_MIN = -1
const HEIGHT_MAX = 2
const HEIGHT_OFFSET_TO_ZERO = 1
const HEIGHT_LAYER_MIN := 20
const HEIGHT_LAYER_MAX := 31

var _error_names = [
	"OK",
	"FAILED",
	"ERR_UNAVAILABLE",
	"ERR_UNCONFIGURED",
	"ERR_UNAUTHORIZED",
	"ERR_PARAMETER_RANGE_ERROR",
	"ERR_OUT_OF_MEMORY",
	"ERR_FILE_NOT_FOUND",
	"ERR_FILE_BAD_DRIVE",
	"ERR_FILE_BAD_PATH",
	"ERR_FILE_NO_PERMISSION",
	"ERR_FILE_ALREADY_IN_USE",
	"ERR_FILE_CANT_OPEN",
	"ERR_FILE_CANT_WRITE",
	"ERR_FILE_CANT_READ",
	"ERR_FILE_UNRECOGNIZED",
	"ERR_FILE_CORRUPT",
	"ERR_FILE_MISSING_DEPENDENCIES",
	"ERR_FILE_EOF",
	"ERR_CANT_OPEN",
	"ERR_CANT_CREATE",
	"ERR_QUERY_FAILED",
	"ERR_ALREADY_IN_USE",
	"ERR_LOCKED",
	"ERR_TIMEOUT",
	"ERR_CANT_CONNECT",
	"ERR_CANT_RESOLVE",
	"ERR_CONNECTION_ERROR",
	"ERR_CANT_ACQUIRE_RESOURCE",
	"ERR_CANT_FORK",
	"ERR_INVALID_DATA",
	"ERR_INVALID_PARAMETER",
	"ERR_ALREADY_EXISTS",
	"ERR_DOES_NOT_EXIST",
	"ERR_DATABASE_CANT_READ",
	"ERR_DATABASE_CANT_WRITE",
	"ERR_COMPILATION_FAILED",
	"ERR_METHOD_NOT_FOUND ",
	"ERR_LINK_FAILED",
	"ERR_SCRIPT_FAILED",
	"ERR_CYCLIC_LINK",
	"ERR_INVALID_DECLARATION",
	"ERR_DUPLICATE_SYMBOL",
	"ERR_PARSE_ERROR",
	"ERR_BUSY",
	"ERR_SKIP",
	"ERR_HELP",
	"ERR_BUG",
	"ERR_PRINTER_ON_FIRE"
]

# call_defered with seconds instead of 1 tick
func _my_call_deferred(time: float, target: Object, method: String, args := []) -> SceneTreeTimer:
	var timer := get_tree().create_timer(time)
	print_if_error(timer.connect("timeout", target, method, args))
	return timer

func kmh2pxs(kmh: float) -> float:
	return kmh / 0.36

func pxs2kmh(pxs: float) -> float:
	return pxs * 0.36

# set all bits in mask that are one in "bits" to the value
func set_bits_to(mask: int, bits: int, value: bool) -> int:
	if value:
		return mask | bits
	else:
		return mask & ~bits

# return if at least one bit in mask that are one in "bits" is one
func is_one_bit_one(mask: int, bits: int) -> bool:
	return mask & bits != 0

# return if all bits in mask that are one in "bits" are one
func are_bits_one(mask: int, bits: int) -> bool:
	return mask & bits == bits

func set_bit_at(mask: int, index: int, value: bool) -> int:
	if value:
		return mask | (1 << index)
	else:
		return mask & ~(1 << index)

func get_bit_at(mask: int, index: int) -> bool:
	return mask & (1 << index) != 0

# print error name if not ok
func print_if_error(error):
	if error != OK:
		if 0 < error and error < _error_names.size():
			printerr(_error_names[error])
		else:
			printerr("Error: ", error)

func erase_all(array: Array, object):
	var i = 0
	var shift = 0
	while i < array.size():
		if array[i] == object:
			shift += 1
		else:
			array[i-shift] = array[i]
		i += 1
	array.resize(array.size() - shift)

func sum(array) -> float:
	var sum = 0.0;
	for val in array:
		sum += val
	return sum
	
func avg(array) -> float:
	return sum(array) / array.size()

func min(array: Array) -> float:
	if array.empty():
		return 0.0
	else:
		var m = array[0]
		for i in range(1, array.size()):
			m = min(m, array[i])
		return m

# round height
func int_height(height: float) -> int:
	return int(round(clamp(height, HEIGHT_MIN, HEIGHT_MAX)))

# get layer that should be tester for the corresponding height
func to_collision_layer(height: float) -> Dictionary:
	height = int_height(height) + HEIGHT_OFFSET_TO_ZERO
	var layer = HEIGHT_LAYER_MIN + height * 3
	return {
		"min" : max(layer - 1, HEIGHT_LAYER_MIN),
		"layer" : layer,
		"max" : min(layer + 1, HEIGHT_LAYER_MAX)
	}

func to_z(height: float) -> int:
	return int_height(height) * 3

func to_link_z(height: float) -> int:
	return to_z(height)

func to_path_z(height: float) -> int:
	return to_z(height) + 1 + 20

func to_vehicle_z(height: float) -> int:
	return to_z(height) + 2
