extends Polygon2D

class_name PathDisplay

export var width := 10.0
export var uv_factor = 0.2

var _tesselated : PoolVector2Array
var _curve : Curve2D

func _ready():
	 material = material.duplicate()

func set_color(color: Color):
	material.set_shader_param("base_color", color)

func set_line(p0: Vector2, p1: Vector2, start: float = 0, end: float = 0):
	var curve = Curve2D.new()
	curve.add_point(p0)
	curve.add_point(p1)
	set_curve(curve, start, end)

func set_curve(curve: Curve2D, start: float = 0, end: float = 0):
	_curve = curve
	_tesselated = PoolVector2Array(_curve.tessellate(3))
	
	# uv > 1
	material.set_shader_param("path_length", max(1, _curve.get_baked_length()))
	material.set_shader_param("start_z", start)
	material.set_shader_param("end_z", end)

func draw():
	var origins = PoolVector2Array()
	origins.resize(_tesselated.size() * 2)
		
	for i in _tesselated.size():
		var j = origins.size() - i - 1
		var dir = _tesselated[min(i+1,_tesselated.size() - 1)] - _tesselated[max(0,i-1)]
		var side : Vector2 = dir.normalized().rotated(PI / 2)
				
		origins[i] = _tesselated[i] + side * width * 0.5
		origins[j] = _tesselated[i] - side * width * 0.5
		
	# fix broken concave (most of the time)
	origins = Geometry.intersect_polygons_2d(Geometry.convex_hull_2d(origins), origins)[0]
	
	var uvs = PoolVector2Array()
	uvs.resize(origins.size())
	
	for i in origins.size():
		uvs[i] = Vector2(0, _curve.get_closest_offset(origins[i]))
		origins[i] = to_local(origins[i])
	polygon = origins
	uv = uvs
