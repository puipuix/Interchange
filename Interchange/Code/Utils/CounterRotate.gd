tool
extends Node2D

# stay straight
func _process(_delta):
	if get_parent() is Node2D:
		rotation = -get_parent().rotation
