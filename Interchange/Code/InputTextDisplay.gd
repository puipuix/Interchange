tool
extends RichTextLabel

export(Color) var input_color = Color.red
export(Color) var action_color = Color.cyan

export var raw_text = "Use %s to %s."
export var input_value = "None"
export var action_value = "None"

func _ready():
	bbcode_enabled = true
	_update_text()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Engine.editor_hint:
		_update_text()

# format the text, will keep + and / white
func _update_text():
	var ip = "[color=#%s]" % input_color.to_html(false)
	var ac = "[color=#%s]" % action_color.to_html(false)
	ip += "%s[/color]" % input_value.replace("/","[/color] / " + ip).replace("+","[/color] + " + ip)
	ac += "%s[/color]" % action_value.replace("/","[/color] / " + ac).replace("+","[/color] + " + ac)
	bbcode_text = raw_text % [ip, ac]
