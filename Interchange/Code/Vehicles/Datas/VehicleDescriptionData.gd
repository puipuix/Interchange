extends Node

class_name VehicleDescriptionData

# auto
var front_wheel_position = 0.0
# auto
var back_wheel_position = 0.0

# greatest distance from the center of rotation of the vehicle
export var radius = 20.0

# what speed is concidered to fast
export var high_speed = 150.0
# < 1, steering is multiplied by that factor at high speed
export(float, 0.0,1.0) var high_speed_steering_factor = 0.2

export(float, 0.0, 90.0) var steering_angle = 60.0

export var mass = 1028

export var friction_force = 0.1
export var engine_force = 16.2
export var reverse_force_factor = 0.5
export var breaking_force = 40

func _ready():
	steering_angle = deg2rad(steering_angle)
	front_wheel_position = get_parent().get_node("FrontWheel").position.x
	back_wheel_position = get_parent().get_node("BackWheel").position.x

# interpolate steering according to the velocity
func get_steering_angle_at_speed(velocity: float):
	return lerp(steering_angle, steering_angle * high_speed_steering_factor, min(1, velocity / MyTools.kmh2pxs(high_speed)))
