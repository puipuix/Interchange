extends Node

class_name VehicleDrivingInputData

var steering := 0.0
var accelerating := 0.0
var breaking := 0.0
var height := 0.0
var blink_left := false
var blink_right := false
var lights := false
var reverse := false
var stopped := false

func get_steering() -> float: return clamp(steering, -1, 1)
func get_accelerating() -> float: return clamp(accelerating, 0, 1)
func get_breaking() -> float: return clamp(breaking, 0, 1)
