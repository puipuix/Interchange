extends Node

# Physics of the vehicle
class_name VehiclePhysicData

export var min_speed_to_reverse := 0.1
export var min_speed_to_move := 0.5

var velocity := Vector2.ZERO
var velocity_scalar := 0.0
var reversing := false

# can we accelerate
# reverse is do we want to reverse
# return not (reversing xor reverse)
func allow_engine(reverse: bool) -> bool:
	return (reversing and reverse) or ((not reversing) and (not reverse))

# forward(1) or backward(-1)
func get_direction() -> int:	
	return -1 if reversing else 1
	
# if not moving, reverse
func try_set_reversing(reverse: bool):
	if (velocity_scalar < min_speed_to_reverse):
		reversing = reverse

# is speed not 0
func is_moving() -> bool:
	return velocity_scalar > min_speed_to_move
