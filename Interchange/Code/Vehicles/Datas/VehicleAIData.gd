extends Node

# Store data for the AI
class_name VehicleAIData

# use to know when a vehicle have reached the next node
export var distance_to_reach_node := 40.0
# how far the vehicle start looking for the path
export var offset_ahead := 50.0

# how far it will look ahead for collision / curve
export var anticipation_time := 5.0

# split the curve by segment of this length
export var anticipation_step_distance := 35.0

# PID
export var acceleraing_P := 0.1
export var breaking_P := 0.05

export var min_speed : int = 10
export var max_speed : int = 150

export(float, EASE) var curve_to_slowdown = 1.0
export(float, EASE) var close_to_slowdown = 1.0

# angle between forward vector and the direction to...
export var angle_to_blink := PI / 12
export var angle_to_priority := PI / 6

var _path : Array
var _path_index : int

var want_to_stop = false
var speed_limit : int = 150 # of nodes
var target_speed = 0.0
var running = true
var spawn_time = 0.0
var despawn_time = 0.0

func set_up(path: Array, vehicle: VehicleBaseEntity, initial_velocity: float, _spawn_time: float) -> int:
	_path_index = 0
	_path = path
	spawn_time = _spawn_time
	if is_ready():
		var link = path[0] as PathLink
		var pos = link.origin.global_position
		var start = get_next_point(pos)
		vehicle.global_position = pos
		vehicle.rotation = vehicle.forward_vector().angle_to(pos.direction_to(start))
		var physic : VehiclePhysicData = vehicle.get_node_or_null("VehiclePhysicData")
		if physic != null:
			physic.velocity_scalar = initial_velocity
			physic.velocity = vehicle.forward_vector() * initial_velocity
		return OK
	else:
		return ERR_INVALID_DATA

func has_next_link() -> bool:
	if _path == null:
		return false
	else:
		return not _path.empty() and _path_index < _path.size() - 1

func has_previous_link() -> bool:
	if _path == null:
		return false
	else:
		return not _path.empty() and _path_index > 0

func has_travel_left():
	return _path_index < _path.size()

func is_arrived():
	return not _path.empty() and _path_index >= _path.size()

# is there a vehicle that have the priority on us and that is not blocked
func need_to_yield(vehicle : VehicleBaseEntity, at: PathLink):
	var input : VehicleDrivingInputData = vehicle.get_node("VehicleDrivingInputData")
	var mask = at.origin.priority_to
	var space = vehicle.get_world_2d().direct_space_state
	var in_vec = at.get_in_vector()
	var param = Physics2DShapeQueryParameters.new()
	param.set_shape(vehicle.view)
	param.exclude = [vehicle]
	param.transform.origin = at.origin.global_position + in_vec * vehicle.view.extents.x
	param.transform.x = in_vec
	# find vehicles
	var result = cast(space, vehicle.height, param, 32)
	# for all vehicles
	for collider in result:
		var veh = collider.collider
		if veh.has_node("VehicleDrivingInputData"):
			var ip : VehicleDrivingInputData = veh.get_node("VehicleDrivingInputData")
			if not ip.stopped: # else ignore
				var layer = 0
				var dir = at.origin.global_position.direction_to(veh.global_position)
				var angle = in_vec.angle_to(dir)
				# find the bitmask layer corresponding to where the vehicle is
				# and where we want to go
				if angle < -angle_to_priority:
					if input.blink_left and not input.blink_right:
						layer = PathNode.PriorityTo.LL
					elif input.blink_right and not input.blink_left:
						layer = PathNode.PriorityTo.LR
					else:
						layer = PathNode.PriorityTo.LF
				elif angle > angle_to_priority:
					if input.blink_left and not input.blink_right:
						layer = PathNode.PriorityTo.RL
					elif input.blink_right and not input.blink_left:
						layer = PathNode.PriorityTo.RR
					else:
						layer = PathNode.PriorityTo.RF
				else:
					if input.blink_left and not input.blink_right:
						layer = PathNode.PriorityTo.FL
					elif input.blink_right and not input.blink_left:
						layer = PathNode.PriorityTo.FR
					else:
						layer = PathNode.PriorityTo.FF
				# if one bit of the layer and the priority mask match, so we need to yield
				if mask & layer != 0:
					return true
	return false

func is_in_intersection(vehicle: VehicleBaseEntity) -> bool:
	# we are in only if away from a node
	if is_right_before_previous_node(vehicle):
		return false
	else:
		return get_next_node().is_merge()

func is_at_intersection(vehicle: VehicleBaseEntity) -> bool:
	return is_right_before_previous_node(vehicle) and get_next_node().is_merge()

func is_near_intersection(_vehicle: VehicleBaseEntity) -> bool:
	# ignore next node -> is_at_intersection
	if has_next_link():
		return get_next_node(1).is_merge()
	# if there are no link after we can't stop
	else:
		return false

func get_sign_node(pos: Vector2) -> PathNode:
	if is_near_previous_node(pos):
		return get_previous_node()
	else:
		return get_next_node()


# can we go forward ?
func is_ready():
	if _path == null:
		return false
	else:
		return not _path.empty() and has_travel_left()

# return { points, heights }
func interpolate_points(pos: Vector2, velocity: float, step: float, height = 0) -> Dictionary:
	var points = [pos]
	var heights = [height]
	
	# how far we will look
	var distance = velocity * anticipation_time
	var off_dist = 0
	
	# we might look multiple curve
	var p_i = _path_index
	var curve : Curve2D = _path[p_i].curve
	
	# look for offset
	var off_add = curve.get_closest_offset(pos) + offset_ahead
	if off_add > curve.get_baked_length():
		off_add -= curve.get_baked_length()
		p_i += 1
		
	# while there is a path and not too far
	while p_i < _path.size() and off_dist <= distance:
		curve = _path[p_i].curve # update curve
		points.append(curve.interpolate_baked(off_add))
		if curve.get_baked_length() > 0:
			heights.append(lerp(_path[p_i].origin.height, _path[p_i].destination.height, off_add / curve.get_baked_length()))
		else:
			heights.append(_path[p_i].origin.height)
			
		off_dist += step
		off_add += step
		# if outside of this curve
		if off_add > curve.get_baked_length():
			off_add -= curve.get_baked_length()
			p_i += 1
	
	
	return { "points" : points, "heights": heights }

func get_path_lenght() -> float:
	if _path == null:
		return 0.0	
	else:
		var sum = 0.0
		for link in _path:
			sum += link.curve.get_baked_length()
		return sum

# distance of anticipation
func get_distance(velocity: float):
	return max(10, velocity * anticipation_time)

# intersect shape
func cast(space : Physics2DDirectSpaceState, height: float, param: Physics2DShapeQueryParameters, count: int) -> Array:
	var layer = MyTools.to_collision_layer(height)
	param.collision_layer = 0
	for l in range(layer.min, layer.max + 1):
		param.collision_layer = MyTools.set_bit_at(param.collision_layer, l, true)
	
	return space.intersect_shape(param, count)
	
func get_nearest_obstacle(velocity: float, vehicle: VehicleBaseEntity) -> Node2D:
	var vehicle_length = vehicle.shape.extents.x
	var data = interpolate_points(vehicle.global_position, velocity, vehicle_length, vehicle.height)
	var points = data.points
	var heights = data.heights
	var space = vehicle.get_world_2d().direct_space_state
	var param = Physics2DShapeQueryParameters.new()
	param.set_shape(vehicle.shape)
	param.exclude = [vehicle]
	# follow the path to check if there is a vehicle before us
	for i in range(1, points.size()):
		param.transform.origin = (points[i-1] + points[i]) / 2.0
		param.transform.x = points[i-1].direction_to(points[i])
		var result = cast(space, heights[i], param, 1)
		if not result.empty():
			return result[0].collider
	return null

# return a value between 0 and 1, 1 being a very curvy path
func get_curviness(pos: Vector2, velocity: float) -> float:
	var data = interpolate_points(pos, velocity, anticipation_step_distance)
	var points = data.points
	if points.size() > 2:
		var curvy = 0
		# points[i] = direction from i  to i + 1
		points[0] = points[0].direction_to(points[1])
		for i in range(1, points.size() - 1):
			points[i] = points[i].direction_to(points[i + 1])
			# dot production between the previous dir and the current dir
			curvy = max(curvy, 1 - max(0, points[i-1].dot(points[i])))
			
		return curvy
	else:
		return 0.0
	
func get_link(off_index: int = 0) -> PathLink:
	return _path[clamp(_path_index + off_index, 0, _path.size() - 1)]

func get_height(_pos: Vector2) -> float:
	var link = get_link()
	#var curve : Curve2D = link.curve
	#var w = 0
	#if curve.get_baked_length() > 0:
	#	w = curve.get_closest_offset(pos) / curve.get_baked_length()
	#return lerp(link.origin.height, link.destination.height, w)
	return max(link.origin.height, link.destination.height)

# get closest path point
func get_next_point(pos: Vector2) -> Vector2:
	var curve : Curve2D = get_link().curve
	var offset = curve.get_closest_offset(pos)
	# if the point is the last of the curve, we look the next curve
	if offset + offset_ahead > curve.get_baked_length() and has_next_link():
		return get_link(1).curve.interpolate_baked(offset + offset_ahead - curve.get_baked_length())
	else:
		return curve.interpolate_baked(offset + offset_ahead)

func is_near_next_node(pos: Vector2) -> bool:
	return pos.distance_squared_to(get_next_node().global_position) < distance_to_reach_node * distance_to_reach_node

func is_right_before_previous_node(vehicle: VehicleBaseEntity) -> bool:
	if is_near_previous_node(vehicle.global_position):
		return vehicle.forward_vector().dot(vehicle.global_position.direction_to(get_previous_node().global_position)) > 0.0
	else:
		return false

func is_right_after_previous_node(vehicle: VehicleBaseEntity) -> bool:
	if is_near_previous_node(vehicle.global_position):
		return vehicle.forward_vector().dot(vehicle.global_position.direction_to(get_previous_node().global_position)) <= 0.0
	else:
		return false

func is_near_previous_node(pos: Vector2) -> bool:
	return pos.distance_squared_to(get_previous_node().global_position) < distance_to_reach_node * distance_to_reach_node

func get_previous_node() -> PathNode:
	return get_link().origin as PathNode

func get_next_node(off_index: int = 0) -> PathNode:
	return get_link(off_index).destination as PathNode

func get_speed_limit():
	if speed_limit < 0:
		return max_speed
	else:
		return speed_limit

# if at the next point, increment the path index and update the speed limit
func update_index(pos: Vector2, forward: bool = true):
	if forward and is_near_next_node(pos):
		_path_index = int(min(_path_index + 1, _path.size()))
	elif not forward and is_near_previous_node(pos):
		_path_index = int(max(_path_index - 1, 0))
	
	var previous = get_previous_node()
	if previous.speed_limit > 0:
		speed_limit = previous.speed_limit
