extends KinematicBody2D

class_name VehicleBaseEntity

enum Fading {NONE, IN, OUT}

var height := 0.0

export var fade_out_time := 1.0
export var fade_in_time := 0.5

onready var _sprite = $Sprite
onready var shape: RectangleShape2D = $CollisionShape.shape
onready var view: RectangleShape2D = $View.shape

var _fading = Fading.NONE

static func get_layer():
	return 2

func _ready():
	_sprite.material = _sprite.material.duplicate()

func fade_in():
	_sprite.self_modulate.a = 0
	_fading = Fading.IN

func fade_and_remove():
	_fading = Fading.OUT

# self modulate to fade in and out
func _process(delta):
	if _fading == Fading.IN:
		_sprite.self_modulate.a = min(1, _sprite.self_modulate.a + delta / fade_in_time)
		if _sprite.self_modulate.a == 1:
			_fading = Fading.NONE
	if _fading == Fading.OUT:
		_sprite.self_modulate.a = max(0, _sprite.self_modulate.a - delta / fade_out_time)
		if _sprite.self_modulate.a == 0:
			_fading = Fading.NONE
			get_parent().remove_child(self)

func _pick_random_color():
	var colors = [Color.blue, Color.white, Color.dimgray, Color.red, Color.yellow]
	var color = colors[randi() % colors.size()]
	set_material_param("base_color", color)

# update z index and collision layer
func set_height(h: float):
	height = h
	z_index = MyTools.to_vehicle_z(h)
	var layer = MyTools.to_collision_layer(h)
	for i in range(MyTools.HEIGHT_LAYER_MIN, layer.min):
		set_collision_layer_bit(i, false)
		set_collision_mask_bit(i, false)
	for i in range(layer.min, layer.max + 1):
		set_collision_layer_bit(i, true)
		set_collision_mask_bit(i, true)
	for i in range(layer.max + 1, MyTools.HEIGHT_LAYER_MAX + 1):
		set_collision_layer_bit(i, false)
		set_collision_mask_bit(i, false)
		
func forward_vector() -> Vector2:
	return transform.x

func backward_vector() -> Vector2:
	return -transform.x
	
func left_vector() -> Vector2:
	return -transform.y
	
func right_vector() -> Vector2:
	return transform.y

func set_material_param(param: String, value):
	_sprite.material.set_shader_param(param, value)
