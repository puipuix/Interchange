extends KinematicBody2D

# deprecated
class_name MyVehicle

const WHEEL_BASE = 25

export var controllable : bool = false

export var DEBUG : bool = false

# DETECTIONS
export var look_ahead : float = 200
export var heading_force : float = 0.1
export var look_path_ahead_distance : float = 400
export var look_path_ahead_count : int = 10
export var default_offset : float = 50
export var interest_power : float = 4

# STEERING
export var steering_angle : float = 1
export var high_speed_steering_factor : float = 0.25
export var high_speed : float = 200

# FORCES
export var friction : float = 0.9
export var engine_power : float = 300
export var breaking_power : float = 200

# SPEED
export(float, EASE) var obstacle_to_breaking = 1.0
export(float, EASE) var obstacle_to_accelerating = 1.0
export var full_break_for_steering : float = 0.5

var path : PathFollow2D

# path following
var facing_obstacle = 0;
var path_transforms = []

var parameters = Physics2DShapeQueryParameters.new()

#movements
var steering = 0
var breaking = 0
var accelerating = 0
var velocity_scalar = 0
var velocity = Vector2.ZERO
var acceleration = Vector2.ZERO

func forward_vector() -> Vector2:
	return transform.x
	
func left_vector() -> Vector2:
	return -transform.y
	
func right_vector() -> Vector2:
	return transform.y

# Called when the node enters the scene tree for the first time.
func _ready():
	path_transforms.resize(look_path_ahead_count)
	parameters.set_shape($Shape.shape)
	parameters.exclude = [self]
	for i in look_path_ahead_count:
		path_transforms[i] = Transform2D()

func _set_path_transforms():
	var offset =  path.get_parent().curve.get_closest_offset(global_position)
	for i in look_path_ahead_count:
		path.offset = offset + i * look_path_ahead_distance / look_path_ahead_count
		path_transforms[i] = Transform2D(path.transform)

func _set_interest():
	#for i in num_rays:
	#	interest[i] = pow(max(0,ray_directions[i].dot(Vector2.RIGHT.rotated(steering))),interest_power)
	pass
	
func _set_steering():
	var curve := path.get_parent().curve as Curve2D
	var point = curve.get_closest_point(global_position)
	var path_direction = path_transforms[0].x + (global_position.direction_to(point) * global_position.distance_squared_to(point) * heading_force * heading_force)
	path_direction = path_direction.normalized()
	path_direction = path_direction.rotated(-rotation)
	steering = clamp(path_direction.angle(),-1.0,1.0)
	
func _set_safe():
	# Cast rays to find safe directions
	var space_state = get_world_2d().direct_space_state
	facing_obstacle = 0
	for i in look_path_ahead_count:
		parameters.transform = path_transforms[i]
		var result = space_state.intersect_shape(parameters, 1)
		if not result.empty():
			result = result[0]
			var close = 1.0 - i / look_path_ahead_count
			facing_obstacle = close
			break

func _set_breaking():
	breaking = 0
	var d = 1
	for i in look_path_ahead_count:
		d = min(d, forward_vector().dot(path_transforms[i].x))
	breaking = smoothstep(1,full_break_for_steering, d) + ease(facing_obstacle, obstacle_to_breaking)
	
func _set_acceleration():
	accelerating = 1 - ease(facing_obstacle, obstacle_to_accelerating)


func _get_AI_inputs():
	_set_path_transforms()
	_set_steering()
	_set_interest()
	_set_safe()
	_set_breaking()
	_set_acceleration()

# PLAYER
func _get_player_inputs():
	accelerating = 1 if Input.is_action_pressed("ui_up") else 0
	breaking = 1 if Input.is_action_pressed("ui_down") else 0
	steering = (-1 if Input.is_action_pressed("ui_left") else 0) + (1 if Input.is_action_pressed("ui_right") else 0)

# PHYSICS
func _apply_driving_input(delta):
	acceleration = forward_vector() * (accelerating * engine_power - breaking * breaking_power)
	
	if velocity_scalar > 0 or true:
		var rear_wheel = position - forward_vector() * WHEEL_BASE / 2.0
		var front_wheel = position + forward_vector() * WHEEL_BASE / 2.0
		rear_wheel += velocity * delta

		var steer = clamp(steering, -steering_angle, steering_angle)
		steer = lerp(steering, steering * high_speed_steering_factor, min(1, velocity_scalar / high_speed))
		front_wheel += velocity.rotated(steer) * delta
		var new_heading = (front_wheel - rear_wheel).normalized()
		velocity = new_heading * velocity_scalar
		rotation = new_heading.angle() 

func _apply_friction():
	if velocity_scalar < 0:
		velocity = Vector2.ZERO
	var friction_force = velocity * friction
	if velocity_scalar < 100:
		friction_force *= 3
	acceleration -= friction_force

func _process(delta):
	if controllable:
		_get_player_inputs()
	elif path:
		_get_AI_inputs()
	if DEBUG:
		update()

func _physics_process(delta):
	_apply_driving_input(delta)
	_apply_friction()
	velocity = move_and_slide(velocity + acceleration * delta)
	velocity_scalar = velocity.length()

func _draw():
	if DEBUG:
		if path and not controllable:
			var offset =  path.get_parent().curve.get_closest_offset(global_position)
			for i in look_path_ahead_count:
				path.offset = offset + look_path_ahead_distance * (i + 1) / look_path_ahead_count
				draw_circle(to_local(path.position), 5, Color.blue)
			draw_line(Vector2.ZERO, forward_vector().rotated(steering - rotation) * 200, Color.yellow, 3)
