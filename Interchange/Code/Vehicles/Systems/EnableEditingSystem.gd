extends SystemBase

class_name EnableEditingSystem

var edit = true

var node_type = SceneLoader.NODE
var selected = null
var old_link = null

onready var _node_editor : NodeEditor = get_tree().root.find_node("NodeEditor", true, false)
onready var _link_editor : LinkEditor = get_tree().root.find_node("LinkEditor", true, false)
onready var _vehicle_display : VehicleDisplay = get_tree().root.find_node("VehicleDisplay", true, false)
onready var _camera : MyCamera = get_tree().root.find_node("MyCamera", true, false)
onready var _ui : UI = get_tree().root.find_node("UI", true, false)


func _ready():
	one_shot = true

func _cast(layer: int):
	var space : Physics2DDirectSpaceState = _level.get_world_2d().direct_space_state
	var results = space.intersect_point(_level.get_global_mouse_position(), 10, [], layer, true, true)
	if results.empty():
		return null
	else:
		var b = results[0].collider
		for i in range(1, results.size()):
			var t = results[i].collider
			if t.z_index > b.z_index:
				b = t
		return b

func _create_link(from, to):
	var link = from.create_link_to(to, old_link)
	select(to)
	old_link = link

func _create_and_selection():
	var node : PathNode = _cast(PathNode.get_layer())
	if node != null:
		# if one was selected and we click on an other one, we create a link
		if node != selected and selected is PathNode:
			_create_link(selected, node)
	else:
		# no target, we add a new node
		node = SceneLoader.instance(node_type)
		_level.add_child(node)
		node.global_position = node.get_global_mouse_position()
		# if one was selected, we create a link
		if selected is PathNode:
			node.set_height(selected.height)
			_create_link(selected, node)
		else:
			select(node)

func _selection(layer: int):
	var node = _cast(layer)
	# no add, so select only
	if node != null:
		# target was selected so unselect if it's a short click
		# (prevent unselect while dragging node)
		if node == selected:
			unselect()
		else:
			# new target so select
			select(node)
	else:
		# no target so unselect
		unselect()

func _drag_and_selection():
	var node = _cast(DragableNode.get_layer())
	# no add, so select only
	if node != null:
		if node.start_follow_mouse():
			select(node)

func stop_running():
	ask_update()
	edit = true
	
func run():
	ask_update()
	edit = false
	unselect()

func _process(_delta):
	if not _ui.is_mouse_above_ui():
		if edit:
			var h = 0
			if not  Input.is_action_pressed("ctrl"):
				if Input.is_action_just_pressed("action_plus"): h += 1
				if Input.is_action_just_pressed("action_minus"): h -= 1
			
			if Input.is_action_just_pressed("move_object"):
				_drag_and_selection()
			elif Input.is_action_just_pressed("select_object"):
				if Input.is_action_pressed("ctrl"):
					_create_and_selection()
				else:
					_selection(PathNode.get_layer() | PathLink.get_layer())
			elif Input.is_action_just_pressed("remove_object"):
				if selected != null:
					var ok = true
					if selected is PathNode:
						ok = not selected.locked
					if ok:
						var s = selected
						unselect()
						s.remove()
			elif h != 0 and selected != null:
				if selected.has_method("incr_height"):
					selected.incr_height(h)
		else:
			if Input.is_action_just_pressed("select_object"):
				_selection(VehicleBaseEntity.get_layer())
			elif Input.is_action_just_pressed("remove_object"):
				if selected is VehicleBaseEntity:
					var s = selected
					unselect()
					s.fade_and_remove()

func _select_node(node: PathNode):
	node.blink = true
	selected = node
	_node_editor.set_node(node)
	
func _unselect_node():
	selected.blink = false
	selected = null
	_node_editor.set_node(null)

func _select_vehicle(node: VehicleBaseEntity):
	selected = node
	_camera.move_to(node)
	_vehicle_display.set_vehicle(node)
	
func _unselect_vehicle():
	selected = null
	_camera.move_to(null)
	_vehicle_display.set_vehicle(null)

func _select_link(node: PathLink):
	node.blink = true
	selected = node
	_link_editor.set_link(node)
	
func _unselect_link():
	selected.blink = false
	selected = null
	_link_editor.set_link(null)

func unselect():
	old_link = null
	if selected is PathNode:
		_unselect_node()
	elif selected is VehicleBaseEntity:
		_unselect_vehicle()
	elif selected is PathLink:
		_unselect_link()

func select(node: Node):
	unselect()
	if node is PathNode:
		_select_node(node)
	elif node is VehicleBaseEntity:
		_select_vehicle(node)
	elif node is PathLink:
		_select_link(node)

func _accept(node: Node) -> bool:
	return node is DragableNode or node is VehicleBaseEntity or node is PathLink

func _update(node: Node, _delta: float):
	if node is DragableNode:
		node.set_displaying(edit)
	elif node is PathLink:
		node.set_control_visible(edit)
	elif node is VehicleBaseEntity:
		if edit:
			node.fade_and_remove()
