extends SystemBase

class_name VehicleSystemBase

func _update(node: Node, delta: float):
	_update_vehicle(node, delta)

func _accept(node: Node) -> bool:
	return node is VehicleBaseEntity

# OVERRIDE

func _update_vehicle(_vehicle: VehicleBaseEntity, _delta: float):
	pass
