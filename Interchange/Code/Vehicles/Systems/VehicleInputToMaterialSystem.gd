extends VehicleSystemBase

class_name VehicleInputToMaterialSystem

func _ready():
	_with_all = ["VehicleDrivingInputData"]

func _update_vehicle(vehicle: VehicleBaseEntity, _delta: float):
	var input : VehicleDrivingInputData = vehicle.get_node("VehicleDrivingInputData")
	vehicle.set_material_param("lights", input.lights)
	vehicle.set_material_param("blink_left", input.blink_left)
	vehicle.set_material_param("blink_right", input.blink_right)
	vehicle.set_material_param("brakes", input.breaking > 0.2)
