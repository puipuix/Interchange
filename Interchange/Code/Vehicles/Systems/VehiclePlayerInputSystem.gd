extends VehicleSystemBase

class_name VehiclePlayerInputSystem

func _ready():
	_with_all = ["VehicleDrivingInputData", "VehiclePlayerTag"]

var _oa := false
var _oe := false
var _ol := false
var _or := false

func _update_vehicle(vehicle: VehicleBaseEntity, _delta: float):
	var input : VehicleDrivingInputData = vehicle.get_node("VehicleDrivingInputData")
	input.accelerating = 1 if Input.is_key_pressed(KEY_Z) else 0
	input.breaking = 1 if Input.is_key_pressed(KEY_S)else 0
	input.steering = (-1 if Input.is_key_pressed(KEY_Q) else 0) + (1 if Input.is_key_pressed(KEY_D) else 0)
	input.height = 0.0;
	if Input.is_key_pressed(KEY_A) and !_oa:
		input.blink_left = !input.blink_left
	if Input.is_key_pressed(KEY_E) and !_oe:
		input.blink_right = !input.blink_right
	if Input.is_key_pressed(KEY_L) and !_ol:
		input.lights = !input.lights
	if Input.is_key_pressed(KEY_R) and !_or:
		input.reverse = !input.reverse
		
	_oa = Input.is_key_pressed(KEY_A)
	_oe = Input.is_key_pressed(KEY_E)
	_ol = Input.is_key_pressed(KEY_L)
	_or = Input.is_key_pressed(KEY_R)
	
