extends VehicleSystemBase

class_name NodeDebugSystem

func _accept(node: Node) -> bool:
	return node is PathNode

func _process(_delta):
	Singleton.FPS.display("NODES", _nodes.size())
