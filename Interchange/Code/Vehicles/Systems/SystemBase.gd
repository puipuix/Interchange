extends Node

# find nodes and update them
class_name SystemBase

onready var _level = get_node("../../Level")

var _nodes := []
var _with_all := []
var _with_none := []

export var use_fixed = true
export var enabled = true
# disable after one flush
export var one_shot = false

func _ready():
	MyTools.print_if_error(get_tree().connect("node_added", self, "_on_node_added"))
	MyTools.print_if_error(get_tree().connect("node_removed", self, "_on_node_removed"))
	for n in _level.get_children():
		_on_node_added(n)

func _on_node_removed(node: Node):
	if _accept(node):
		_nodes.erase(node)
		_on_erased(node)
	
func _on_node_added(node: Node):
	if _accept(node):
		_nodes.append(node)
		_on_added(node)

# update each node that have the needed subnodes
func _flush(delta):
	for n in _nodes:
		var ok = true
		for sub in _with_all:
			ok = ok && n.has_node(sub)
		for sub in _with_none:
			ok = ok && !n.has_node(sub)
		if ok:
			_update(n, delta)
	enabled = enabled and not one_shot
	
func _physics_process(delta):
	if enabled and use_fixed:
		_flush(delta)

func _process(delta):
	if enabled and not use_fixed:
		_flush(delta)

func ask_update():
	enabled = true

func ask_stop():
	enabled = false

# TO OVERRIDE

func _accept(_node: Node) -> bool:
	return false

func _update(_node: Node, _delta: float):
	pass

func _on_added(_node: Node):
	pass
	
func _on_erased(_node: Node):
	pass
