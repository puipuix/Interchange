extends VehicleSystemBase

class_name VehicleAIInputSystem

onready var _process_timer : ProcessTimer = get_tree().root.find_node("ProcessTimer", true, false)

func _ready():
	_with_all = ["VehicleDrivingInputData", "VehicleAIData", "VehiclePhysicData", "VehicleDescriptionData"]
	_with_none = ["VehiclePlayerTag"]

func _set_lights(vehicle: VehicleBaseEntity, input: VehicleDrivingInputData, data: VehicleAIData):
	# look if there is an intersection ahead
	var angle = 0
	if data.has_next_link() and data.get_next_node().is_diverge():
		var link = data.get_link()
		var a1 = link.get_out_vector().angle_to(data.get_link(1).get_in_vector())
		var a2 = link.get_out_vector().angle_to(data.get_link(1).get_linear_vector())
		angle = a1 if abs(a1) > abs(a2) else a2
	elif data.is_in_intersection(vehicle) or data.is_at_intersection(vehicle):
		var link = data.get_link()
		angle = link.get_in_vector().angle_to(link.get_linear_vector())
	elif data.is_near_intersection(vehicle):
		var link = data.get_link(1)
		angle = link.get_in_vector().angle_to(link.get_linear_vector())
		
		
	input.blink_left = angle < -data.angle_to_blink
	input.blink_right = angle > data.angle_to_blink
	
	
	input.lights = false
	input.reverse = false

# steer toward the next point
func _set_steering(vehicle: VehicleBaseEntity, input: VehicleDrivingInputData, data: VehicleAIData, desc: VehicleDescriptionData, physic : VehiclePhysicData):
	var v_pos = vehicle.global_position
	var p_pos = data.get_next_point(vehicle.global_position)
	if v_pos != p_pos:
		var dir = v_pos.direction_to(p_pos)
		var steer = vehicle.forward_vector().angle_to(dir) / desc.get_steering_angle_at_speed(physic.velocity_scalar)
		input.steering = clamp(steer, -1, 1)

# accelerate to break to reach the target speed
func _aim_for_speed(target_speed: float, input: VehicleDrivingInputData, physic: VehiclePhysicData, data: VehicleAIData):
	var diff = abs(target_speed - physic.velocity_scalar)
	if physic.velocity_scalar > target_speed:
		input.accelerating = 0
		input.breaking = data.breaking_P * diff
	else:
		input.breaking = 0
		input.accelerating = data.acceleraing_P * diff

# look the path curve to know if we need to slow down
func _get_path_speed(vehicle: VehicleBaseEntity, data: VehicleAIData, physic: VehiclePhysicData) -> float:
	var curvy = data.get_curviness(vehicle.global_position, physic.velocity_scalar)
	var speed = lerp(data.max_speed,data.min_speed,ease(curvy, data.curve_to_slowdown))
	return MyTools.kmh2pxs(speed)

# return the target speed to not reach the target
# the target can be a static object or a moving one
func _speed_to_not_reach(physic: VehiclePhysicData, data: VehicleAIData, pos: Vector2, target: Vector2, stop_distance : float = 0, target_velocity: Vector2 = Vector2.ZERO) -> float:
	
	# distance to target
	var distance = max(0, pos.distance_to(target) - stop_distance)
	# how far we are looking at
	var anticipation = data.get_distance(physic.velocity_scalar)
	# how close are we from the target
	# if high distance -> w == 0
	var w = ease(1 - distance / anticipation, data.close_to_slowdown)
	
	var speed = 0
	
	# problem if we are not moving
	if physic.velocity_scalar > 0:
		# find relative velocity
		var no_collision_velocity = target_velocity.project(physic.velocity)
		if physic.velocity.dot(no_collision_velocity) <= 0:
			speed = 0
		else:
			speed = no_collision_velocity.length()
	else:
		speed = target_velocity.length()
		
	# lerp to ignore if far away
	return lerp(MyTools.kmh2pxs(data.get_speed_limit()),speed,w)

# speed limit from nodes
func _get_speed_limit(vehicle: VehicleBaseEntity, data: VehicleAIData, physic: VehiclePhysicData, desc: VehicleDescriptionData) -> float:
	var next_node = data.get_next_node()
	# if the next node does not have a limit
	if next_node.speed_limit < 0:
		# read old limit
		return MyTools.kmh2pxs(data.get_speed_limit())
	else:
		# else we slow down or accelerate to reach the next speed limit
		return _speed_to_not_reach(physic, data, vehicle.global_position, next_node.global_position, desc.radius, vehicle.forward_vector() * MyTools.kmh2pxs(next_node.speed_limit))

# slow down if something is on the path
func _get_collision_speed(vehicle: VehicleBaseEntity, data: VehicleAIData, physic: VehiclePhysicData, desc: VehicleDescriptionData) -> float:
	var obstacle = data.get_nearest_obstacle(physic.velocity_scalar, vehicle)
	if obstacle != null and physic.velocity_scalar > 0:
		var obj_vel = Vector2.ZERO
		var obj_rad = 0.0
		if obstacle.has_node("VehiclePhysicData"):
			obj_vel = obstacle.get_node("VehiclePhysicData").velocity
		if obstacle.has_node("VehicleDescriptionData"):
			obj_rad = obstacle.get_node("VehicleDescriptionData").radius
		return _speed_to_not_reach(physic, data, vehicle.global_position, obstacle.global_position, obj_rad + desc.radius + 20, obj_vel)
	else:
		return MyTools.kmh2pxs(data.max_speed)

# slow down for priorities
func _get_traffic_speed(vehicle: VehicleBaseEntity, data: VehicleAIData, physic: VehiclePhysicData, desc: VehicleDescriptionData, input: VehicleDrivingInputData) -> float:
	var sign_node = data.get_sign_node(vehicle.global_position)
	var max_speed = data.max_speed
	var should_stop = false
	input.stopped = false
	
	# if in we need to move out if it
	# else
	if not data.is_in_intersection(vehicle):
		var check_priority = false
		input.stopped = not data.is_at_intersection(vehicle)
		if sign_node.priority_type != PathNode.PriorityType.NONE:
			if sign_node.priority_type == PathNode.PriorityType.LIGHTS:
				if PathNode.LightsState.RED == sign_node.lights_state or PathNode.LightsState.YELLOW == sign_node.lights_state:
					# no need to check priority
					should_stop = true
					input.stopped = true
				else:
					check_priority = true
			elif sign_node.priority_type == PathNode.PriorityType.STOP:
				# try to stop until reach the intersection and not moving
				if data.is_at_intersection(vehicle):
					if data.want_to_stop and not physic.is_moving():
						data.want_to_stop = false
				else:
					data.want_to_stop = true
				
				max_speed = 10
				if data.want_to_stop:
					# no need to check priority as we need to stop
					should_stop = true
					check_priority = false
					input.stopped = true
				else:
					check_priority = true
			elif sign_node.priority_type == PathNode.PriorityType.YIELD:
				# slow down and check
				max_speed = 10
				check_priority = true
			elif sign_node.priority_type == PathNode.PriorityType.PRIORITY:
				# check only
				check_priority = true
		
		if check_priority:
			if data.has_next_link():
				# check the current link
				if data.is_at_intersection(vehicle):
					should_stop = data.need_to_yield(vehicle, data.get_link())
				else:
					# node not reached so check next link
					should_stop = data.need_to_yield(vehicle, data.get_link(1))
				input.stopped = should_stop or (not data.is_at_intersection(vehicle))
			else:
				# if the travel is complete there are not need to check
				should_stop = false
				input.stopped = false
	
	if should_stop:
		var speed = _speed_to_not_reach(physic, data, vehicle.global_position, sign_node.global_position, desc.radius)
		return speed
	else:
		return MyTools.kmh2pxs(max_speed)

func _update_vehicle(vehicle: VehicleBaseEntity, _delta: float):
	var input : VehicleDrivingInputData = vehicle.get_node("VehicleDrivingInputData")
	var data : VehicleAIData = vehicle.get_node("VehicleAIData")
	var physic : VehiclePhysicData = vehicle.get_node("VehiclePhysicData")
	var desc : VehicleDescriptionData = vehicle.get_node("VehicleDescriptionData")
	
	if not data.is_ready():
		input.breaking = 1
		input.steering = 0
		input.accelerating = 0
		input.height = 0.0
		input.blink_right = true
		input.blink_left = true
		input.stopped = true
		data.target_speed = 0.0
		if data.running and data.is_arrived():
			data.despawn_time = _process_timer.physics_time
			vehicle.fade_and_remove()
			data.running = false
	else:
		data.update_index(vehicle.global_position)
		if data.has_travel_left():
			# needed first for traffic speed
			_set_lights(vehicle, input, data)
			
			var n_speed = _get_speed_limit(vehicle, data, physic, desc)
			var p_speed = _get_path_speed(vehicle, data, physic)
			var c_speed = _get_collision_speed(vehicle, data, physic, desc)
			var r_speed =  _get_traffic_speed(vehicle, data, physic, desc, input)
			# select the slowest speed
			data.target_speed = MyTools.min([
				n_speed, 
				c_speed, 
				p_speed, 
				r_speed
				])
							
			_set_steering(vehicle, input, data, desc, physic)
			_aim_for_speed(data.target_speed, input, physic, data)
			
			input.height = data.get_height(vehicle.global_position)

