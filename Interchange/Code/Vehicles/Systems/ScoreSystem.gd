extends SystemBase

class_name ScoreSystem

export var flow_factor = 100.0
export var concrete_factor = 0.001
export var complexity_factor = 0.5
# score will be calculated after x seconds
export var min_sim_time = 60.0

onready var _score_display : ScoreDisplay = get_tree().root.find_node("ScoreDisplay", true, false)
onready var _process_timer : ProcessTimer = get_tree().root.find_node("ProcessTimer", true, false)

var _waiting = 0
var _time_max = 10
var _time_count = 0
var _time = 0

var _flow_max = 1000
var _flow_count = 0
var _flow = 0
var _cpx = 0
var _cct = 0

func _accept(_node: Node) -> bool:
	return _node is VehicleBaseEntity or _node is PathNode or _node is PathLink

func _flush(delta):
	_waiting = 0 # reset before counting
	._flush(delta)
	# those who need to spawn are not moving
	if _waiting > 0:
		_flow = (_flow * _flow_count + 0.0) / (_flow_count + _waiting)
		# clamp to forget old values
		_flow_count = min(_flow_max, _flow_count + _waiting)
	_update_ui()

func _update(_node: Node, _delta: float):
	if _node is PathNodeSpawner: 
		# count how many are waiting
		_waiting += _node._waiting.size()
	elif _node is VehicleBaseEntity:
		# average speed
		var physic : VehiclePhysicData = _node.get_node("VehiclePhysicData")
		_flow = (_flow * _flow_count + physic.velocity_scalar) / (_flow_count + 1)
		# clamp to forget old values
		_flow_count = min(_flow_max, _flow_count + 1)
	
func _update_ui():
	_score_display.set_complexity(_cpx)
	_score_display.set_time(_process_timer.physics_time)
	_score_display.set_concrete(_cct)
	_score_display.set_waiting(_waiting)
	if _time_count > 0:
		_score_display.set_flow_time(_time)
	else:
		_score_display.set_flow_time(-1)
		
	if _flow_count > 0:
		_score_display.set_flow_speed(_flow)
	else:
		_score_display.set_flow_speed(-1)
		
	if _process_timer.physics_time > min_sim_time:
		_score_display.set_score(_flow * flow_factor / max(1, _cct * concrete_factor + _cpx * complexity_factor))
	else:
		_score_display.set_score(-1)
		
func ask_update():
	.ask_update()
	_score_display.visible = true
	_flow_count = 0
	_flow = 0
	_flow_count = 0
	_time = 0
	_cpx = 0
	_cct = 0
	# update concrete use and complexity
	for node in _nodes:
		if node is PathNode:
			_cpx += max(0, node.get_in_count() - 1) + max(0, node.get_out_count() - 1)
		elif node is PathLink:
			_cct += node.get_concrete()
	_update_ui()

func ask_stop():
	.ask_stop()
	_score_display.visible = false
	
func _on_erased(_node: Node):
	if _node is VehicleBaseEntity:
		# if a vehicle disapear, calculate it's lifespan
		var ai : VehicleAIData = _node.get_node("VehicleAIData")
		var life = ai.despawn_time - ai.spawn_time
		if life > 0:
			_time = (_time * _time_count + life) / (_time_count + 1)
			# clamp to forget old values
			_time_count = min(_time_max, _time_count + 1)
			
			
