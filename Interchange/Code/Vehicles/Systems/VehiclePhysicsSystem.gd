extends VehicleSystemBase

class_name VehiclePhysicsSystem

export var min_velocity_to_move := 0.001
export var high_friction_velocity := 1.0

func _ready():
	_with_all = ["VehicleDrivingInputData", "VehicleDescriptionData", "VehiclePhysicData"]

func _update_vehicle(vehicle: VehicleBaseEntity, delta: float):
	var input : VehicleDrivingInputData = vehicle.get_node("VehicleDrivingInputData")
	var desc : VehicleDescriptionData = vehicle.get_node("VehicleDescriptionData")
	var physic : VehiclePhysicData = vehicle.get_node("VehiclePhysicData")
	
	physic.try_set_reversing(input.reverse)
	var dir = physic.get_direction()
	
	# WHEEL
	var back_wheel = vehicle.forward_vector() * desc.back_wheel_position;
	var front_wheel = vehicle.forward_vector() * desc.front_wheel_position;

	back_wheel += physic.velocity * delta;

	var steer = input.get_steering() * desc.get_steering_angle_at_speed(physic.velocity_scalar);

	# front wheel make the vehicle steer
	front_wheel += physic.velocity.rotated(steer * dir) * delta
	
	var heading = (front_wheel - back_wheel).normalized()
	physic.velocity = heading * physic.velocity_scalar;
	
	
	# FORCES reverse_force_factor
	var acceleration = 0
	
	if physic.allow_engine(input.reverse):
		acceleration = input.get_accelerating() * desc.engine_force
		if (physic.reversing):
			# slower when reverse
			acceleration *= desc.reverse_force_factor
	
	acceleration -= input.get_breaking() * desc.breaking_force
	
	var friction = physic.velocity_scalar * desc.friction_force;
	if physic.velocity_scalar < high_friction_velocity:
		friction *= 3
	acceleration -= friction
	
	acceleration *= delta
	# MOVE if acceleration is enough
	if physic.velocity_scalar + acceleration < min_velocity_to_move:
		physic.velocity = Vector2.ZERO
		physic.velocity_scalar = 0
	else:
		
		physic.velocity += vehicle.forward_vector() * acceleration
		vehicle.set_height(input.height)
		var result = vehicle.move_and_slide(physic.velocity * dir) * dir
		physic.velocity = result
		
		physic.velocity_scalar = physic.velocity.length()
		
		# if no collision, align with velocity
		if vehicle.get_slide_count() == 0 and physic.velocity_scalar > min_velocity_to_move:
			# Align car rotation to the velocity
			vehicle.rotation = physic.velocity.angle()
