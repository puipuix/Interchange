extends VehicleSystemBase

class_name VehicleDebugSystem

export var show_speed := false

func _ready():
	_with_all = ["VehiclePhysicData"]

func _process(_delta):
	Singleton.FPS.display("VHCLS", _nodes.size())
	
func _update_vehicle(vehicle: VehicleBaseEntity, _delta: float):
	if show_speed:
		var physic : VehiclePhysicData = vehicle.get_node("VehiclePhysicData")
		print(vehicle.name,": ", MyTools.pxs2kmh(physic.velocity_scalar), "km/h")
