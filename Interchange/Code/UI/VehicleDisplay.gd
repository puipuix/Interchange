extends MarginContainer

class_name VehicleDisplay

var _vehicle : VehicleBaseEntity
var _phy : VehiclePhysicData
var _ip : VehicleDrivingInputData
var _ai : VehicleAIData
var _is_busy = false

onready var _name = find_node("Name")
onready var _s = find_node("S")
onready var _t = find_node("T")
onready var _a = find_node("A")
onready var _b = find_node("B")
onready var _st = find_node("St")
onready var _rmv = find_node("Remove")
onready var _text = find_node("Texture")

func _ready():
	set_vehicle(null)

func set_vehicle(vehicle: VehicleBaseEntity):
	_vehicle = vehicle
	if _vehicle != null:
		_name.text = "Vehicle [%.0f]" % _vehicle.get_instance_id() 
		_phy = _vehicle.get_node_or_null("VehiclePhysicData")
		_ai = _vehicle.get_node_or_null("VehicleAIData")
		_ip = _vehicle.get_node_or_null("VehicleDrivingInputData")
		_text.material = _vehicle._sprite.material
		_text.texture = _vehicle._sprite.texture
	else:
		_phy = null
		_ip = null
		_ai = null

func _process(_delta):
	visible = _vehicle != null
	if visible:
		if not is_instance_valid(_vehicle):
			set_vehicle(null)
		elif not _vehicle.is_inside_tree():
			set_vehicle(null)
		else:
			if _ai != null:
				_t.value = MyTools.pxs2kmh(_ai.target_speed)
			if _phy != null:
				_s.value = MyTools.pxs2kmh(_phy.velocity_scalar)
			if _ip != null:
				_a.value = _ip.accelerating * 100
				_b.value = _ip.breaking * 100
				_st.pressed = _ip.stopped

func _on_Remove_pressed():
	if not _is_busy and visible:
		_vehicle.fade_and_remove()

