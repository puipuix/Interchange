extends MarginContainer

class_name NodeEditor

var _node : PathNode
var _is_busy = false

onready var _name = find_node("Name")

onready var _M_Y = find_node("M_Y")
onready var _M_L = find_node("M_L")
onready var _M_A = find_node("M_A")
onready var _M_SP = find_node("M_SP")

onready var _M_P : NodePositionEditor = find_node("NodePositionEditor")
onready var _type = find_node("Type")
onready var _speed = find_node("Speed")

onready var _a = find_node("A")

onready var _sp_min = find_node("SpawnMin")
onready var _sp_max = find_node("SpawnMax")

onready var _RR = find_node("RR")
onready var _RF = find_node("RF")
onready var _RL = find_node("RL")
onready var _FR = find_node("FR")
onready var _FF = find_node("FF")
onready var _FL = find_node("FL")
onready var _LR = find_node("LR")
onready var _LF = find_node("LF")
onready var _LL = find_node("LL")

onready var _green = find_node("Green")
onready var _yellow = find_node("Yellow")
onready var _red = find_node("Red")
onready var _offset = find_node("Offset")

# convert UI id to speed
var _speed_convert = {
	0 : -1,
	1 : 30,
	2 : 50,
	3 : 70,
	4 : 90,
	5 : 110,
	6 : 130,
	-1 : 0,
	30 : 1,
	50 : 2,
	70 : 3,
	90 : 4,
	110 : 5,
	130 : 6
}

func _ready():
	set_node(null)

func set_node(node: PathNode):
	_node = node
	_M_P.set_node(_node)
	_update_UI()

func _update_UI():
	visible = _node != null
	if visible:
		_name.text = "Node [%.0f]" % _node.get_instance_id()
		_is_busy = true # disable signals
		_type.selected = _node.priority_type
		_type.disabled = _node.locked
		
		if _speed_convert.has(_node.speed_limit) and (_node.speed_limit < 0 or _node.speed_limit > 10):
			_speed.selected = _speed_convert[_node.speed_limit]
		else:
			_speed.selected = 0
		
		_speed.disabled = _node.locked
		
		_M_A.visible = _node is PathNodeDespawner
		_M_SP.visible = _node is PathNodeSpawner
		
		if _node is PathNodeDespawner:
			_a.editable = not _node.locked
			_a.value = _node.attractiveness
		
		if _node is PathNodeSpawner:
			_sp_min.editable = not _node.locked
			_sp_max.editable = not _node.locked
			
			_sp_min.value = _node.min_spawn_time
			_sp_max.value = _node.max_spawn_time
		
		if _node.priority_type == PathNode.PriorityType.NONE:
			_M_Y.visible = false
			_M_L.visible = false
		else:
			_M_Y.visible = true
			var p = _node.priority_to
			_RR.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.RR)
			_RF.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.RF)
			_RL.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.RL)
			_FR.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.FR)
			_FF.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.FF)
			_FL.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.FL)
			_LR.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.LR)
			_LF.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.LF)
			_LL.pressed = MyTools.are_bits_one(p, PathNode.PriorityTo.LL)
			
			_RR.disabled = _node.locked
			_RF.disabled = _node.locked
			_RL.disabled = _node.locked
			_FR.disabled = _node.locked
			_FF.disabled = _node.locked
			_FL.disabled = _node.locked
			_LR.disabled = _node.locked
			_LF.disabled = _node.locked
			_LL.disabled = _node.locked
			
			if _node.priority_type == PathNode.PriorityType.LIGHTS:
				_M_L.visible = true
				_green.max_value = 60
				_yellow.max_value = 60
				_red.max_value = 60
				_offset.max_value = 60
				
				_green.value = _node.green_time
				_yellow.value = _node.yellow_time
				_red.value = _node.red_time
				_offset.value = _node.offset_time
				
				_green.editable = not _node.locked
				_yellow.editable = not _node.locked
				_red.editable = not _node.locked
				_offset.editable = not _node.locked
				
			else:
				_M_L.visible = false
		_is_busy = false

func _on_Type_selected(index):
	if not _is_busy and visible:
		_node.set_priority_type(index)
		_update_UI()

func _on_bit_toggled():
	if not _is_busy and visible:
		var p = 0
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.RR, _RR.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.RF, _RF.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.RL, _RL.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.FR, _FR.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.FF, _FF.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.FL, _FL.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.LR, _LR.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.LF, _LF.pressed)
		p = MyTools.set_bits_to(p, PathNode.PriorityTo.LL, _LL.pressed)
		_node.priority_to = p
		_update_UI()


func _on_Green_changed(value):
	if not _is_busy and visible:
		_node.green_time = int(clamp(value, 0, 60))
		_node.yellow_time = int(clamp(_node.yellow_time, 0, 60 - _node.green_time))
		_node.red_time = int(clamp(_node.red_time, 0, 60 - _node.green_time - _node.yellow_time))
		_update_UI()	

func _on_Yellow_changed(value):
	if not _is_busy and visible:
		_node.yellow_time = int(clamp(value, 0, 60))
		_node.green_time = int(clamp(_node.green_time, 0, 60 - _node.yellow_time))
		_node.red_time = int(clamp(_node.red_time, 0, 60 - _node.green_time - _node.yellow_time))
		_update_UI()

func _on_Red_changed(value):
	if not _is_busy and visible:
		_node.red_time = int(clamp(value, 0, 60))
		_node.yellow_time = int(clamp(_node.yellow_time, 0, 60 - _node.red_time))
		_node.green_time = int(clamp(_node.green_time, 0, 60 - _node.red_time - _node.yellow_time))
		_update_UI()

func _on_Offset_changed(value):
	if not _is_busy and visible:
		_node.offset_time = int(clamp(value, 0, 60))
		_update_UI()

func _on_Speed_selected(index):
	if not _is_busy and visible:
		_node.speed_limit = _speed_convert[index] # get the speed from id
		_update_UI()

func _on_Attr_changed(value):
	if not _is_busy and visible and _node is PathNodeDespawner:
		_node.attractiveness = value
		_update_UI()


func _on_SpawnMin_changed(value):
	if not _is_busy and visible and _node is PathNodeSpawner:
		_node.min_spawn_time = value
		_node.max_spawn_time = max(value, _sp_max.value)
		_update_UI()


func _on_SpawnMax_changed(value):
	if not _is_busy and visible and _node is PathNodeSpawner:
		_node.min_spawn_time = min(value, _sp_min.value)
		_node.max_spawn_time = value
		_update_UI()
