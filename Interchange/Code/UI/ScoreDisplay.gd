extends MarginContainer

class_name ScoreDisplay

onready var _concrete = find_node("Concrete")
onready var _time = find_node("Time")
onready var _complexity = find_node("Complexity")
onready var _flow_speed = find_node("AvgSpeed")
onready var _flow_time = find_node("AvgTime")
onready var _score = find_node("Score")
onready var _waiting = find_node("Waiting")

func to_text(format: String, value: float):
	if value > 0:
		return format % value
	else:
		return "-"

func set_time(value: float):
	var m = floor(value / 60.0)
	var s = fmod(value, 60)
	_time.text = "%.0f:%02.0f" % [m, s]

func set_concrete(value: float):
	_concrete.text = "%.0f$" % value
	
func set_complexity(value: float):
	_complexity.text = "%.0f" % value

func set_flow_speed(value: float):
	_flow_speed.text = to_text("%.0f km/h", MyTools.pxs2kmh(value))
	
func set_flow_time(value: float):
	_flow_time.text = to_text("%.0fs", value)

func set_score(value: float):
	_score.text = to_text("%.0f", value)
	
func set_waiting(value: float):
	_waiting.text = "%.0f vehicles" % value
