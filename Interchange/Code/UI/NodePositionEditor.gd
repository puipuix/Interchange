extends MarginContainer

class_name NodePositionEditor

var _node : PathNode
var _is_busy = false

onready var _X = find_node("X")
onready var _Y = find_node("Y")
onready var _H = find_node("H")

func _ready():
	set_node(null)

func set_node(node: PathNode):
	if _node != null:
		_node.disconnect("position_changed", self, "_update_UI")
	_node = node
	if _node != null:
		MyTools.print_if_error(_node.connect("position_changed", self, "_update_UI"))
	_update_UI()

func _update_UI():
	if _node != null:
		_is_busy = true # disable signals
		
		_X.value = _node.global_position.x
		_Y.value = _node.global_position.y
		_H.value = _node.height
		
		_X.editable = not _node.locked
		_Y.editable = not _node.locked
		_H.editable = not _node.locked
		_is_busy = false

func _on_pos_value_changed(_value):
	if not _is_busy and visible:
		_node.move_to(_X.value, _Y.value, _H.value)
		# _update_UI called by signal
