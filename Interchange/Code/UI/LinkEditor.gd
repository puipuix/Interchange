extends MarginContainer

class_name LinkEditor

var _link : PathLink
var _is_busy = false

onready var _name = find_node("Name")
onready var _M_O : NodePositionEditor = find_node("OriginPositionEditor")
onready var _M_D : NodePositionEditor = find_node("DestinationPositionEditor")

onready var _bend = find_node("Bend")
onready var _straighten = find_node("Straighten")

func _ready():
	set_link(null)

func set_link(link: PathLink):
	_link = link
	if _link != null:
		_M_O.set_node(_link.origin)
		_M_D.set_node(_link.destination)
	else:
		_M_O.set_node(null)
		_M_D.set_node(null)
	_update_UI()

func _update_UI():
	visible = _link != null
	if visible:
		_name.text = "Link [%.0f]" % _link.get_instance_id()

func _on_Bend_pressed():
	if not _is_busy and visible:
		_link.bend()

func _on_Straighten_pressed():
	if not _is_busy and visible:
		_link.straighten()
