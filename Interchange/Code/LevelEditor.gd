
extends Node

onready var _level = $Level
onready var _camera = $MyCamera
onready var _edit_system : EnableEditingSystem = find_node("EnableEditingSystem")
onready var _score_system : ScoreSystem = find_node("ScoreSystem")
onready var _run : MenuButton = find_node("Run")
onready var _edit : MenuButton = find_node("Edit")
onready var _file : MenuButton = find_node("File")
onready var _help : MenuButton = find_node("Help")
onready var _file_explorer : FileDialog = find_node("FileDialog")
onready var _help_dialog : WindowDialog = find_node("HelpWindowDialog")
onready var _about_dialog : WindowDialog = find_node("AboutWindowDialog")
onready var _process_timer : ProcessTimer = $ProcessTimer
onready var _ui : UI = $UI

var _speed = 1.0
var _current_path = ""

func _ready():
	AStarManager.find_all_in(_level)
	Singleton.FPS = find_node("GDEFPS")
	
	# copy example files to use folder
	_file_explorer.current_dir = OS.get_user_data_dir()
	var dir = Directory.new()
	var levels = ["stop.json", "lights.json", "round.json", "highway.json"]
	for level in levels:
		var path = "user://%s" % level
		if not dir.file_exists(path):
			MyTools.print_if_error(dir.copy("res://Content/Levels/%s" % level, path))

func update_title(tag = ""):
	OS.set_window_title("Interchange: " + _file_explorer.current_file + tag)

# start or stop simulation
func _simulation(on: bool, fast: bool):
	if on:
		if fast:
			_speed = 5.0
		else:
			_speed = 1.0
		Engine.time_scale = _speed
		_process_timer.restart()
		_edit_system.run()
		_score_system.ask_update()
	else:
		Engine.time_scale = 1.0
		_edit_system.stop_running()
		_score_system.ask_stop()
		_process_timer.stop()
		_process_timer.reset()

func _process(_delta):
	if not _ui.is_mouse_above_ui() and Input.is_action_just_pressed("switch_edit_mode"):
		_simulation(_edit_system.edit, true)
	if not _edit_system.edit and Input.is_action_just_pressed("pause"):
		if Engine.time_scale > 0.1:
			Engine.time_scale = 0
		else:
			Engine.time_scale = _speed
		
	_run.get_popup().set_item_disabled(0, not _edit_system.edit)
	_run.get_popup().set_item_disabled(1, not _edit_system.edit)
	_run.get_popup().set_item_disabled(3, _edit_system.edit)
	
	_edit.get_popup().set_item_disabled(0, true)
	_edit.get_popup().set_item_disabled(1, true)
	_edit.get_popup().set_item_disabled(3, not _edit_system.edit)
	_edit.get_popup().set_item_disabled(4, not _edit_system.edit)
	
	_file.get_popup().set_item_disabled(0, not _edit_system.edit)
	_file.get_popup().set_item_disabled(1, not _edit_system.edit)
	_file.get_popup().set_item_disabled(3, not _edit_system.edit)
	_file.get_popup().set_item_disabled(4, not _edit_system.edit)

# remove nodes
func _clear(force = false):
	_edit_system.unselect()
	_edit_system.stop_running()
	_score_system.ask_stop()
	for node in _level.get_children():
		if node is PathNode:
			if not node.locked or force:
				node.call_deferred("remove")

func _save(path: String):
	print("Saving ", path)
	var level = {}
	for node in _level.get_children():
		level[node.get_instance_id()] = node.to_dic()
	
	var data = {
		"version" : 1,
		"level" : level,
	}
	
	var file = File.new()
	var error = file.open(path, File.WRITE)
	MyTools.print_if_error(error)
	if error == OK:
		OS.set_window_title(path)
		file.store_pascal_string(JSON.print(data, "\t"))
		file.close()

func _load(path: String):
	print("Loading ", path)
	var file = File.new()
	var error = file.open(path, File.READ)
	MyTools.print_if_error(error)
	if error == OK:
		OS.set_window_title(path)
		_clear(true)
		var result = JSON.parse(file.get_pascal_string())
		MyTools.print_if_error(result.error)
		if result.error == OK:
			var data : Dictionary = result.result
			var level : Dictionary = data["level"]
			# create instances
			var instances = {}
			for id in level.keys():
				var instance = SceneLoader.instance(level[id]["res"])
				instances[int(id)] = instance
				_level.add_child(instance)
			
			# load data
			for id in level.keys():
				instances[int(id)].from_dic(level[id], instances)
				
			# post load
			for id in level.keys():
				instances[int(id)].post_load()

func _on_file_selected(path):
	_current_path = path
	if _file_explorer.mode == FileDialog.MODE_SAVE_FILE:
		_save(path)
	elif _file_explorer.mode == FileDialog.MODE_OPEN_FILE:
		_load(path)
	update_title()

func _on_File_id_pressed(id):
	if id == 0:
		#new
		_file_explorer.current_file = ""
		_load("res://Content/Levels/empty.json")
		update_title("*")
	elif id == 1:
		# load
		_file_explorer.mode = FileDialog.MODE_OPEN_FILE
		_file_explorer.popup_centered_ratio()
	elif id == 3 and _file_explorer.current_file != "":
		# save
		_file_explorer.mode = FileDialog.MODE_SAVE_FILE
		_on_file_selected(_current_path)
	elif id == 4 or (id == 3 and _file_explorer.current_file == ""):
		# save as
		_file_explorer.mode = FileDialog.MODE_SAVE_FILE
		_file_explorer.current_file = "level.json"
		_file_explorer.popup_centered_ratio()
	if id == 6:
		get_tree().quit()

func _on_Run_id_pressed(id):
	if id == 0:
		_simulation(true, false)
	elif id == 1:
		_simulation(true, true)
	elif id == 3:
		_simulation(false, false)

func _on_Edit_id_pressed(id):
	if id == 0:
		pass #undo
	elif id == 1:
		pass #redo
	elif id == 3:
		for node in _level.get_children():
			if node is PathLink:
				node.call_deferred("remove")
	elif id == 4:
		_clear()
	elif id == 6:
		_edit_system.node_type = SceneLoader.NODE
		_edit.get_popup().set_item_disabled(6, true)
		_edit.get_popup().set_item_disabled(7, false)
		_edit.get_popup().set_item_disabled(8, false)
	elif id == 7:
		_edit_system.node_type = SceneLoader.SPAWN
		_edit.get_popup().set_item_disabled(6, false)
		_edit.get_popup().set_item_disabled(7, true)
		_edit.get_popup().set_item_disabled(8, false)
	elif id == 8: 
		_edit_system.node_type = SceneLoader.DESPAWN
		_edit.get_popup().set_item_disabled(6, false)
		_edit.get_popup().set_item_disabled(7, false)
		_edit.get_popup().set_item_disabled(8, true)
	elif id == 10:
		_camera.global_position = Vector2.ZERO

func _on_Help_id_pressed(id):
	if id == 0:
		_about_dialog.popup_centered_ratio()
	elif id == 1:
		_help_dialog.popup_centered_ratio()
