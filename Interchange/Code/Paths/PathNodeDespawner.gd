extends PathNode

class_name PathNodeDespawner

# how much vehicle will want to get here
export(int, 1, 10) var attractiveness := 1

func to_dic() -> Dictionary:
	var dic = .to_dic()
	dic["res"] = SceneLoader.DESPAWN
	dic["a"] = attractiveness
	return dic

func from_dic(data: Dictionary, instances: Dictionary):
	.from_dic(data, instances)
	attractiveness = data["a"]
