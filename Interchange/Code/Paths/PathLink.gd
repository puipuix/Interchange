extends Area2D

class_name PathLink

var origin : Node2D
var control0 : PathLinkControl
var control1 : PathLinkControl
var destination : Node2D
var curve : Curve2D
var blink = false
var blink_speed = 2.0
var height : float = 0

onready var _display_road : PathDisplay = $DisplayRoad
onready var _display_c0 : PathDisplay = $DisplayControl0
onready var _display_c1 : PathDisplay = $DisplayControl1
onready var _collision : CollisionPolygon2D = $CollisionPolygon2D

export var control_curve_factor := 2.0
export(float, 0, 0.45) var control_min_distance_factor := 0.25
export var control_angle_to_distance = 0.5

export var in_color := Color(0, 1, 0.5)
export var out_color := Color(1, 0, 0.5)

# called when the control points are moved
signal curve_updated(who)

static func get_layer():
	return 8

# blink using the alpha channel of the node
func _process(_delta):
	if blink:
		_display_road.self_modulate.a = cos(OS.get_system_time_msecs() * 0.001 * blink_speed * PI) * 0.5 + 0.5
	else:
		_display_road.self_modulate.a = 1.0

func set_control_visible(v: bool):
	_display_c0.visible = v
	_display_c1.visible = v

# return the direction to be aligned to the link when entering in it
func get_in_vector() -> Vector2:
	return curve.get_point_out(0).normalized()

# return the direction to be aligned to the link when leaving in out	
func get_out_vector() -> Vector2:
	return -curve.get_point_in(1).normalized()

# return the direction from the origin to the destination
func get_linear_vector() -> Vector2:
	return origin.global_position.direction_to(destination.global_position)

# create and add the control points, connect the signals
func _set_up_final(_origin: Node2D, _destination: Node2D, c0: Vector2, c1: Vector2):
	origin = _origin
	destination = _destination
	control0 = SceneLoader.instance(SceneLoader.CTRL)
	control1 = SceneLoader.instance(SceneLoader.CTRL)
		
	origin.add_child(control0)
	destination.add_child(control1)
	
	control0.set_color(out_color)
	_display_c0.set_color(out_color)
	control1.set_color(in_color)
	_display_c1.set_color(in_color)
	
	MyTools.print_if_error(origin.connect("position_changed", self, "update"))
	MyTools.print_if_error(destination.connect("position_changed", self, "update"))
	MyTools.print_if_error(control0.connect("position_changed", self, "update"))
	MyTools.print_if_error(control1.connect("position_changed", self, "update"))
	
	curve = Curve2D.new()
	
	control0.global_position = c0
	control1.global_position = c1
	
	curve.add_point(origin.global_position)
	curve.add_point(destination.global_position)

# set up to connect the 2 nodes
# if from is not null, will align the first control with it
func set_up(_origin: Node2D, _destination: Node2D, from: PathLinkControl) -> PathLinkControl:
	var c0
	var c1
	
	var p0 = _origin.global_position
	var p1 = _destination.global_position
	var stdir = (p1 - p0)
	var l = stdir.length()
	stdir /= l # normalize
	# set the control point distance relative to the distance between the 2 nodes
	var ctrl_distance = l * control_min_distance_factor
	
	# straight line
	if from == null || _origin.get_out_count() > 0:
		c0 = p0 + stdir * ctrl_distance
		c1 = p1 - stdir * ctrl_distance
	else:
		# curve to align with previous control point
		var prevdir = (p0 - from.global_position).normalized()
		var angle = stdir.angle_to(prevdir)
		# use the same angle between the straight line direction and the control direction
		var dir = stdir.rotated(-angle)
		# increase the distance with the angle
		ctrl_distance = max(ctrl_distance, l * abs(dir.angle_to(prevdir)) / PI * control_angle_to_distance)
		c0 = p0 + prevdir * ctrl_distance
		c1 = p1 - dir * ctrl_distance
	
	_set_up_final(_origin, _destination, c0, c1)
	
	update()
	
	return control1

# automaticaly bend to align with the nodes in and out direction
func bend():
	var p0 = origin.global_position
	var p1 = destination.global_position
	var stdir = (p1 - p0)
	var l = stdir.length()
	stdir /= l # normalize
	
	var in_dir = origin.get_in_direction()
	var out_dir = destination.get_out_direction()
	
	if in_dir == Vector2.ZERO:
		in_dir = stdir
		
	if out_dir == Vector2.ZERO:
		out_dir = stdir
	
	var angle = in_dir.angle_to(out_dir)
	
	var ctrl_distance = l * max(control_min_distance_factor, abs(angle) / PI * control_angle_to_distance)
	control0.global_position = p0 + in_dir * ctrl_distance
	control1.global_position = p1 - out_dir * ctrl_distance
	update()
	
# remove any curve
func straighten():
	var p0 = origin.global_position
	var p1 = destination.global_position
	var stdir = (p1 - p0)
	control0.global_position = p0 + stdir * control_min_distance_factor
	control1.global_position = p1 - stdir * control_min_distance_factor
	update()

# udpate the curve data and the road display
func update():
	height = max(origin.height, destination.height)
	z_index = MyTools.to_link_z(height)
	var p0 = origin.global_position
	var p1 = destination.global_position
	var c0 = control0.global_position
	var c1 = control1.global_position
	
	curve.set_point_position(0, p0)
	curve.set_point_position(1, p1)
		
	var dir0 = (c0 - p0) * control_curve_factor
	var dir1 = (c1 - p1) * control_curve_factor
	
	curve.set_point_in(0, -dir0)
	curve.set_point_out(0, dir0)
	
	curve.set_point_in(1, dir1)
	curve.set_point_out(1, -dir1)
	var tesselated = Array(curve.tessellate(3))
	for i in tesselated.size():
		tesselated[i] = to_local(tesselated[i])
	
	_display_road.set_curve(curve, origin.height, destination.height)
	_display_c0.set_line(p0, c0)
	_display_c1.set_line(p1, c1)
	
	
	_display_road.draw()
	_display_c0.draw()
	_display_c1.draw()
	
	_collision.polygon = _display_road.polygon
	
	origin.auto_rotate()
	destination.auto_rotate()
	
	emit_signal("curve_updated", self)

# increment the height of both nodes
func incr_height(h: float):
	origin.incr_height(h)
	destination.incr_height(h)

# delete, ignore the origin or destination if they are been removed too
func remove(ignore: Node2D = null):
	if ignore != origin:
		origin.remove_link_out(self)
	if ignore != destination:
		destination.remove_link_in(self)
	get_parent().remove_child(self)
	control0.get_parent().remove_child(control0)
	control1.get_parent().remove_child(control1)
	
	origin.disconnect("position_changed", self, "update")
	destination.disconnect("position_changed", self, "update")
	control0.disconnect("position_changed", self, "update")
	control1.disconnect("position_changed", self, "update")
	
	queue_free()
	control0.queue_free()
	control1.queue_free()

# save
func to_dic() -> Dictionary:
	return {
		"res" : SceneLoader.LINK,
		"p0" : origin.get_instance_id(),
		"p1" : destination.get_instance_id(),
		"c0" : var2str(control0.global_position),
		"c1" : var2str(control1.global_position),
	}

# load
func from_dic(data: Dictionary, instances: Dictionary):
	_set_up_final(instances[int(data["p0"])], instances[int(data["p1"])], str2var(data["c0"]), str2var(data["c1"]))

# update curve and display after been loaded
func post_load():
	update()

# return the cost to build this link
func get_concrete():
	var h = (origin.height + destination.height) * 0.5
	var m = abs(h) + 1
	if h < 0:
		m *= 2
	return curve.get_baked_length() * m
