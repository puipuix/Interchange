extends DragableNode

class_name PathLinkControl

func _ready():
	_sprite.material = _sprite.material.duplicate()

func set_color(color: Color):
	_sprite.material.set_shader_param("base_color", color)
