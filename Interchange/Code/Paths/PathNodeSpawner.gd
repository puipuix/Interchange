extends PathNode

class_name PathNodeSpawner

export var min_spawn_time := 5.0
export var max_spawn_time := 10.0
# if the last vehicle is closer to that distance, nothing will spawn
export var free_distance := 50.0


var _waiting = []
var _last_spawn : Node2D = null
var _targets = []
var _next_spawn

var _attractive_targets = []
var _update_attractive = false

func _ready():
	_next_spawn = min_spawn_time
	# detect despawn nodes been added
	MyTools.print_if_error(get_tree().connect("node_added", self, "_on_node_added"))
	MyTools.print_if_error(get_tree().connect("node_removed", self, "_on_node_removed"))
	for n in get_parent().get_children():
		_on_node_added(n)

func _on_node_removed(node: Node):
	if node is PathNodeDespawner:
		_targets.erase(node)

func _on_node_added(node: Node):
	if node is PathNodeDespawner:
		_targets.append(node)

# find a path to a random despawn node
func _find_path_to_random(path: Array) -> bool:
	path.clear()
	# select a random target to go
	var start_index : int = int(rand_range(0, _attractive_targets.size()))
	# use to select an other node if no path if found
	var index_shift : int = 0
	var excluded = []
	# while nothing is founded and there is target to find
	while path.empty() and index_shift < _attractive_targets.size():
		# select a target
		var target = _attractive_targets[(start_index + index_shift) % _attractive_targets.size()]
		# if already tested
		if excluded.has(target):
			index_shift += 1 # increase index
		elif not AStarManager.find_path(self, target, path): # path not found
			index_shift += 1 # increase index
			excluded.append(target) # add to the excluded nodes

	return index_shift < _attractive_targets.size()

func _process(_delta):
	# if visible -> editor
	if displaying:
		_last_spawn = null
		_next_spawn = min_spawn_time
		_waiting.clear()
		_update_attractive = true
	else:
		# just start level, update attractive
		if _update_attractive:
			_update_attractive = false
			_attractive_targets.clear()
			for tgt in _targets:
				# add multiple time to increase the chance to be selected
				for i in tgt.attractiveness:
					_attractive_targets.append(tgt)
			_targets.shuffle()
			
		# can spawn something ?
		var locked = false
		if _last_spawn != null:
			if is_instance_valid(_last_spawn) and _last_spawn.is_inside_tree() and global_position.distance_squared_to(_last_spawn.position) < free_distance * free_distance:
				locked = true
			else:
				_last_spawn = null

		# if it's time to spawn
		if _next_spawn < _process_timer.physics_time:
			# calculate the next spawn time
			_next_spawn = _process_timer.physics_time + rand_range(min_spawn_time, max_spawn_time)
			# find a path
			var path = []
			if _find_path_to_random(path):
				# add to the list of waiting vehicle, saving the spawn time
				_waiting.push_back({"spawn": _process_timer.physics_time, "path": path})

		# if can spawn and want to spawn things
		if not locked and not _waiting.empty():
			# spawn a new car
			var spawn_data = _waiting.pop_front()
			var spawn_time = spawn_data["spawn"]
			var path = spawn_data["path"]
			var vehicle = SceneLoader.instance(SceneLoader.CAR) as VehicleBaseEntity
			get_parent().add_child(vehicle)
			vehicle.fade_in()
			vehicle._pick_random_color()
			var data : VehicleAIData = vehicle.get_node("VehicleAIData")
			MyTools.print_if_error(data.set_up(path, vehicle, MyTools.kmh2pxs(speed_limit), spawn_time))
			_last_spawn = vehicle

func to_dic() -> Dictionary:
	var dic = .to_dic()
	dic["res"] = SceneLoader.SPAWN
	dic["min_s"] = min_spawn_time
	dic["max_s"] = max_spawn_time
	return dic

func from_dic(data: Dictionary, instances: Dictionary):
	.from_dic(data, instances)
	min_spawn_time = data["min_s"] 
	max_spawn_time = data["max_s"]
