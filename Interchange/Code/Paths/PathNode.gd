extends DragableNode

class_name PathNode

onready var _line = $Line

var links_out = []
var links_in = []
export var height := 0.0

export var blink_speed = 4.0
var blink = false


enum PriorityType {NONE=0, PRIORITY=1, STOP=2, YIELD=3, LIGHTS=4 }
export(PriorityType) var priority_type = PriorityType.NONE

enum PriorityTo {NONE=0, RR=1, RF=2, RL=4, FR=8, FF=16, FL=32, LR=64, LF=128, LL=256}
export(PriorityTo, FLAGS) var priority_to := PriorityTo.NONE

export(int, 0, 60) var green_time := 25
export(int, 0, 60) var yellow_time := 3
export(int, 0, 60) var red_time := 30
export(int, 0, 60) var offset_time := 0

enum LightsState {NONE=0,GREEN=1,YELLOW=2,RED=3}
var lights_state : int = LightsState.NONE


export var speed_limit : int = -1

onready var _process_timer : ProcessTimer = get_tree().root.find_node("ProcessTimer", true, false)
onready var _lights : Node2D = $Pivot
var _lights_material : ShaderMaterial

static func get_layer():
	return 4

# update default priority to and display
func set_priority_type(type: int):
	if priority_type != type:
		if type == PriorityType.NONE:
			priority_to = 0
		if type == PriorityType.PRIORITY:
			priority_to = PriorityTo.RF |  PriorityTo.RL | PriorityTo.FL
		elif type == PriorityType.LIGHTS:
			priority_to = PriorityTo.FL
		else:
			priority_to = PriorityTo.RF | PriorityTo.RL |  PriorityTo.FL | PriorityTo.LR | PriorityTo.LF | PriorityTo.LL
	priority_type = type
	
	_lights.visible = priority_type == PriorityType.LIGHTS
	_line.visible = priority_type == PriorityType.STOP or priority_type == PriorityType.YIELD
	if priority_type == PriorityType.STOP:
		_line.material.set_shader_param("split", 1)
	elif priority_type == PriorityType.YIELD:
		_line.material.set_shader_param("split", 5)
		
func _ready():
	# duplicate to have independant materials
	_lights_material = $Pivot/Lights.material.duplicate()
	$Pivot/Lights.material = _lights_material
	_sprite.material = _sprite.material.duplicate()
	$Line.material = $Line.material.duplicate()
	z_index = MyTools.to_path_z(height)
	set_priority_type(priority_type)

func _process(_delta):
# blink using the alpha channel of the node
	if blink:
		_sprite.self_modulate.a = cos(OS.get_system_time_msecs() * 0.001 * blink_speed * PI) * 0.5 + 0.5
	else:
		_sprite.self_modulate.a = 1.0
	
	# update traffic lights
	lights_state = LightsState.NONE
	if _process_timer.started:
		# total time
		var ligts_time = green_time + yellow_time + red_time
		# modulo to keep the current time lower to the total time
		var current_time = fmod(offset_time + _process_timer.physics_time, ligts_time)
		
		# find what color needs to be lighten up
		if current_time < green_time:
			lights_state = LightsState.GREEN
		elif current_time < green_time + yellow_time:
			lights_state = LightsState.YELLOW
		else:
			lights_state = LightsState.RED

	_lights_material.set_shader_param("state", lights_state)

func get_in_count():
	return links_in.size()

func get_out_count():
	return links_out.size()

# align the line and light to the in/out direction offset by 90 degree
func auto_rotate():
	var inout = get_inout_direction()
	var angle = 0
	if inout != Vector2.ZERO:
		angle = inout.angle() + PI / 2
		
	_line.rotation = angle
	_lights.rotation = angle

func remove_link_in(link: PathLink):
	links_in.erase(link)
	auto_rotate()
	
func remove_link_out(link: PathLink):
	links_out.erase(link)
	auto_rotate()

func add_link_from(link: PathLink):
	links_in.append(link)
	auto_rotate()

# create a link and return the control point
func create_link_to(target: PathNode, from: PathLinkControl = null) -> PathLinkControl:
	var link : PathLink = SceneLoader.instance(SceneLoader.LINK)
	get_parent().add_child(link)
	var control = link.set_up(self, target, from)
	links_out.append(link)
	target.add_link_from(link)
	auto_rotate()
	return control

# multiple input links
func is_merge() -> bool:
	return links_in.size() > 1
	
# multiple output links
func is_diverge() -> bool:
	return links_out.size() > 1

func remove():
	for link in links_in:
		link.remove(self)
	for link in links_out:
		link.remove(self)
	links_in.clear()
	links_out.clear()
	get_parent().remove_child(self)
	queue_free()

func incr_height(h: float):
	if not locked:
		set_height(height + h)

func _set_height_no_signal(h: float):
	height = MyTools.int_height(h)
	z_index = MyTools.to_path_z(height)
	
func set_height(h: float):
	_set_height_no_signal(h)
	emit_signal("position_changed")

func move_to(x: float, y: float, h: int):
	var old = Vector3(global_position.x, global_position.y, height)
	global_position = Vector2(x,y)
	_set_height_no_signal(h)
	var n = Vector3(global_position.x, global_position.y, height)
	if old != n:
		emit_signal("position_changed")

# average the out direction of input link and the in direction of output links
# return the average direction of the node
func get_inout_direction() -> Vector2:
	var sum = Vector2.ZERO
	var count = links_in.size() + links_out.size()
	for link in links_in:
		sum += link.get_out_vector()
	for link in links_out:
		sum += link.get_in_vector()
	if count > 0:
		return sum / count
	else:
		return Vector2.ZERO
		
# return the average out direction of input link
func get_in_direction(ignore : PathLink = null) -> Vector2:
	var sum = Vector2.ZERO
	var count = links_in.size()
	for link in links_in:
		if link != ignore:
			sum += link.get_out_vector()
		else:
			count -= 1
	if count > 0:
		return sum / count
	else:
		return Vector2.ZERO

# return the average in direction of output link
func get_out_direction(ignore : PathLink = null) -> Vector2:
	var sum = Vector2.ZERO
	var count = links_out.size()
	for link in links_out:
		if link != ignore:
			sum += link.get_in_vector()
		else:
			count -= 1
	if count > 0:
		return sum / count
	else:
		return Vector2.ZERO

# save
func to_dic() -> Dictionary:
	var lo = []
	var li = []
	for link in links_out:
		lo.append(link.get_instance_id())
	for link in links_in:
		li.append(link.get_instance_id())
	
	return {
		"res" : SceneLoader.NODE,
		"lo" : lo,
		"li" : li,
		"h" : height,
		"xy" : var2str(global_position),
		"p_ty" : priority_type,
		"p_to" : priority_to,
		"s_l" : speed_limit,
		"r_t" : red_time,
		"g_t" : green_time,
		"y_t" : yellow_time,
		"o_t" : offset_time
	}

# load
func from_dic(data: Dictionary, instances: Dictionary):
	for link in data["lo"]:
		links_out.append(instances[int(link)])
	for link in data["li"]:
		links_in.append(instances[int(link)])
	height = data["h"]
	
	global_position = str2var(data["xy"])
	set_priority_type(data["p_ty"])
	priority_to = data["p_to"]
	speed_limit = data["s_l"]
	red_time = data["r_t"]
	green_time = data["g_t"]
	yellow_time = data["y_t"]
	offset_time = data["o_t"]

func post_load():
	pass
