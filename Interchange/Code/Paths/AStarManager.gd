extends Node

# it's not possible to add weight to connection between points
# so link and node will be added to the astar points
# the link point will be at the same coordinate as the destination node point
# and the weight will be applied to the link point
var _astar := AStar2D.new()

# store the links and nodes to find them back from their ID.
var _id_to_link := {}
var _id_to_node := {}

func _ready():
	# detect when a node / link is added or removed from the level
	MyTools.print_if_error(get_tree().connect("node_added", self, "_on_node_added", [], CONNECT_DEFERRED))
	MyTools.print_if_error(get_tree().connect("node_removed", self, "_on_node_removed"))
		
# find all node and link in the level
func find_all_in(node: Node):
	for n in node.get_children():
		_on_node_added(n)

# update the A* point weight corresponding to the link length
func on_curve_updated(who: PathLink):
	var dist = who.origin.global_position.distance_to(who.destination.global_position)
	if dist > 0:
		# As the weight is multiplied to the distance between 2 points, 
		# the weight is the ratio between the curve length and the straight line length.
		# so the straight distance will cancel out.
		_astar.set_point_weight_scale(who.get_instance_id(), max(1, who.curve.get_baked_length() / dist))

func find_path(from: PathNode, to: PathNode, path: Array) -> bool:
	# get the path as an list of node/link ids
	var ids = _astar.get_id_path(from.get_instance_id(), to.get_instance_id())
	# get and keep only the links (each link point is placed where the destination node is)
	for id in ids:
		var link = _id_to_link.get(id, null)
		if link != null:
			path.append(link)
	
	return not path.empty()

func _on_node_removed(node: Node):
	# remove from dictionnaries and astar
	if node is PathNode or node is PathLink:
		if node.has_signal("curve_updated"):
			node.disconnect("curve_updated", self, "on_curve_updated")
		_astar.remove_point(node.get_instance_id())
# warning-ignore:return_value_discarded
		_id_to_link.erase(node.get_instance_id())
# warning-ignore:return_value_discarded
		_id_to_node.erase(node.get_instance_id())
	
func _on_node_added(node: Node):
	# if node or link, add the dictionnaries using their instance id
	if node is PathNode:
		_id_to_node[node.get_instance_id()] = node
		_astar.add_point(node.get_instance_id(), node.global_position)
	elif node is PathLink:
		_id_to_link[node.get_instance_id()] = node
		# set the point where the destination is so the distance between the link and the node is 0
		_astar.add_point(node.get_instance_id(), node.destination.global_position)
		_astar.connect_points(node.origin.get_instance_id(), node.get_instance_id(), false)
		_astar.connect_points(node.get_instance_id(), node.destination.get_instance_id(), false)
		# cancel the straight line length to keep only the curve length
		MyTools.print_if_error(node.connect("curve_updated", self, "on_curve_updated"))
