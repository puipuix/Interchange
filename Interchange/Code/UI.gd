extends CanvasLayer

# tell if the mouse is not above the center node
class_name UI

export(NodePath) var play_zone_path : NodePath
onready var _play_zone : Control = get_node(play_zone_path)

var _mouse_above = false

func _ready():
	MyTools.print_if_error(_play_zone.connect("mouse_entered", self, "_on_mouse_entered"))
	MyTools.print_if_error(_play_zone.connect("mouse_exited", self, "_on_mouse_exited"))

func _on_mouse_entered():
	_mouse_above = false

func _on_mouse_exited():
	_mouse_above = true

func is_mouse_above_ui():
	return _mouse_above
