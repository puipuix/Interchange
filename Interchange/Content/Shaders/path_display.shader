shader_type canvas_item;

uniform vec4 underground_color : hint_color = vec4(vec3(0), 1);
uniform vec4 base_color : hint_color = vec4(vec3(0.20), 1);
uniform vec4 lvl1_color : hint_color = vec4(vec3(0.40), 1);
uniform vec4 lvl2_color : hint_color = vec4(vec3(0.60), 1);
uniform vec4 lvl3_color : hint_color = vec4(vec3(0.80), 1);

uniform float start_z = 0;
uniform float end_z = 0;
uniform float path_length = 1.0;

void fragment()
{
	int s_z = int(round(start_z));
	int e_z = int(round(end_z));
	vec3 start = base_color.rgb;
	if (s_z == -1){
		start = underground_color.rgb;
	} else if (s_z == 1) {
		start = lvl1_color.rgb;
	} else if (s_z == 2) {
		start = lvl2_color.rgb;
	} else if (s_z == 3) {
		start = lvl3_color.rgb;
	}
	vec3 end = base_color.rgb;
	if (e_z == -1){
		end = underground_color.rgb;
	} else if (e_z == 1) {
		end = lvl1_color.rgb;
	} else if (e_z == 2) {
		end = lvl2_color.rgb;
	} else if (e_z == 3) {
		end = lvl3_color.rgb;
	}
	vec2 ruv = vec2(UV.x, UV.y / path_length);
	COLOR.rgb = mix(start, end, smoothstep(0,1, ruv.y));
	//NORMALMAP = vec3(ruv, 0.5);
}