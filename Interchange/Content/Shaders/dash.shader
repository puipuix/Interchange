shader_type canvas_item;

uniform int split : hint_range(1,10) = 10;

void fragment()
{
	vec4 text_color = texture(TEXTURE, UV);
	COLOR.rgb = text_color.rgb;
	float s = float(split - 1);
	float x = UV.x * (s + 0.5);
	if (mod(x,1) > 0.5) {
		COLOR.a = 0.0
	} else {
		COLOR.a = 1.0
	}
}