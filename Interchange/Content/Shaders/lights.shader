shader_type canvas_item;

uniform int state : hint_range(0,3) = 0;

uniform float off_color : hint_range(0.0,1.0) = 0.25;
uniform vec4 R_color : hint_color = vec4(1.0,0.0,0.0, 1.0);
uniform vec4 G_color : hint_color = vec4(0.0,1.0,0.0, 1.0);
uniform vec4 Y_color : hint_color = vec4(1.0,1.0,0.0, 1.0);

uniform float blink_time : hint_range(0.0,2.0) = 1.0;
uniform float blink_on : hint_range(0.0, 1.0) = 0.5;

void fragment()
{
	vec4 text_color = texture(TEXTURE, UV);
	if (text_color.r > 0.95) {
		COLOR = R_color;
		if (state != 3){
			COLOR *= off_color;
		}
	} else if (text_color.g > 0.95) {
		COLOR = G_color;
		if (state != 1){
			COLOR *= off_color;
		}
	} else if (text_color.b > 0.95) {
		COLOR = Y_color;
		if (state != 2 && (state != 0 || mod(TIME / blink_time, 1) < blink_on)){
			COLOR *= off_color;
		}
	} else {
		COLOR = text_color;
	}
	COLOR.a = text_color.a;
}