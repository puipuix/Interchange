shader_type canvas_item;

const vec3 BLINK_ON_COLOR = vec3(1.0,0.5,0.05);
const vec3 BLINK_OFF_COLOR = vec3(0.3,0.15,0.05);
const vec3 BRAKES_ON_COLOR = vec3(1.0,0.05,0.05);
const vec3 BRAKES_OFF_COLOR = vec3(0.2,0.05,0.05);
const vec3 LIGHTS_ON_COLOR = vec3(0.95,0.95,0.5);
const vec3 LIGHTS_OFF_COLOR = vec3(0.25,0.2,0.15);

uniform vec4 base_color : hint_color = vec4(1);
uniform float blink_time : hint_range(0.0,2.0) = 1.0;
uniform float blink_on : hint_range(0.0, 1.0) = 0.5;
uniform bool blink_left;
uniform bool blink_right;
uniform bool brakes;
uniform bool lights;

void fragment()
{
	vec4 text_color = texture(TEXTURE, UV);
	COLOR = text_color * base_color;
	if (text_color.b > 0.95) {
		if (UV.x > 0.5) {
			if (lights)	{
				COLOR.rgb = LIGHTS_ON_COLOR;
			} else {
				COLOR.rgb = LIGHTS_OFF_COLOR;
			}
		} else {
			if (lights)	{
				COLOR.rgb = BRAKES_ON_COLOR;
			} else {
				COLOR.rgb = BRAKES_OFF_COLOR;
			}
		}
	}
	
	if (text_color.g > 0.95) {
		if (brakes)	{
			COLOR.rgb = BRAKES_ON_COLOR;
		} else if (text_color.b <= 0.95) {
			COLOR.rgb = BRAKES_OFF_COLOR;
		}
	}
	
	if (text_color.r > 0.95) {
		if (mod(TIME / blink_time, 1) < blink_on && ((blink_left && UV.y < 0.5) || (blink_right && UV.y > 0.5))) {
			COLOR.rgb = BLINK_ON_COLOR;
		} else {
			if (text_color.b <= 0.95) {
				COLOR.rgb = BLINK_OFF_COLOR;
			}
		}
	}
}