shader_type canvas_item;

uniform vec4 base_color : hint_color = vec4(1);

void fragment()
{
	COLOR.rgba = base_color * texture(TEXTURE, UV);
}